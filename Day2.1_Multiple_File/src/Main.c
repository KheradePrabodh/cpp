#include<stdio.h>

#include "../include/Account.h"
#include "../include/Bank.h"


int menu_list( void )
{
	int choice;
	printf("0.Exit\n");
	printf("1.Create New Account\n");
	printf("2.Deposit\n");
	printf("3.Withdraw\n");
	printf("4.Print Account details\n");
	printf("Enter choice	:	");
	scanf("%d",&choice);
	return choice;
}

int main( void )
{
	int choice, accNumber;
	float balance, amount;
	struct Bank bank = { -1 };
	struct Account account;
	while( ( choice = menu_list( ) ) )
	{
		switch( choice )
		{
		case 1:
			accept_account_info(&account);
			accNumber = create_account(&bank, account);
			print_account_number(accNumber);
			break;
		case 2:
			accept_account_number(&accNumber);
			accept_amount( &amount );
			balance = deposit(&bank, accNumber, amount );
			print_balance(balance);
			break;
		case 3:
			accept_account_number(&accNumber);
			accept_amount(&amount );
			balance = withdraw(&bank, accNumber, amount );
			print_balance(balance);
			break;
		case 4:
			accept_account_number(&accNumber);
			account = get_account_details(&bank, accNumber);
			print_account_info(&account);
			break;
		}
	}
	return 0;
}
