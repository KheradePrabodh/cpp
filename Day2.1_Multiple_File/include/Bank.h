#ifndef BANK_H_
#define BANK_H_

#include "../include/Account.h"

struct Bank
{
	int index;
	struct Account arr[ 5 ];
};

void accept_account_info( struct Account *ptraccount );
int create_account( struct Bank *ptrBank, struct Account account );
float deposit( struct Bank *ptrBank, int number, float amount );
float withdraw( struct Bank *ptrBank, int number, float amount );
struct Account get_account_details( struct Bank *ptrBank, int number );
void print_account_info( struct Account *ptraccount );
void accept_account_number( int *number );
void accept_amount( float *amount );
void print_account_number( int number );
void print_balance( float balance );

#endif /* BANK_H_ */
