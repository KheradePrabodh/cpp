#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( int real = 0, int imag = 0 )
	{
		this->real = real;
		this->imag = imag;
	}
	void printRecord( void )
	{
		cout<<this->real<<"	"<<this->imag<<endl;
	}

	friend Complex operator+=( Complex &c1, Complex &c2 )
	{
		c1.real += c2.real;
		c1.imag += c2.imag;
		return c1;
	}
	friend Complex operator-=( Complex &c3, Complex &c4 )
	{
		c3.real += c4.real;
		c3.imag += c4.imag;
		return c3;
	}
	friend Complex operator*=( Complex &c5, Complex &c6 )
	{
		c5.real += c6.real;
		c5.imag += c6.imag;
		return c5;
	}
	friend Complex operator/=( Complex &c7, Complex &c8 )
	{
		c7.real += c8.real;
		c7.imag += c8.imag;
		return c7;
	}
	friend Complex operator%=( Complex &c9, Complex &c10 )
	{
		c9.real += c10.real;
		c9.imag += c10.imag;
		return c9;
	}
};


int main( void )
{
	Complex c1( 10, 20 );
	Complex c2( 30, 40 );
	cout << "c1	:	";
	c1.printRecord();
	cout << "c2	:	";
	c2.printRecord();
	cout << endl;

	c1 += c2;	//operator+=( c1, c2 );
	cout << "c1+=c2	:	" << endl;
	c1.printRecord();
	c2.printRecord();
	cout << endl;

	Complex c3( 30, 40 );
	Complex c4( 10, 20 );
	cout << "c3	:	";
	c3.printRecord();
	cout << "c4	:	";
	c4.printRecord();
	cout << endl;

	c3 -= c4;	//operator-=( c3, c4 );
	cout << "c3-=c4	:	" << endl;
	c3.printRecord();
	c4.printRecord();
	cout << endl;

	Complex c5( 10, 20 );
	Complex c6( 10, 20 );
	cout << "c5	:	";
	c5.printRecord();
	cout << "c6	:	";
	c6.printRecord();
	cout << endl;

	c5 *= c6;	//operator*=( c5, c6 );
	cout << "c5*=c6	:	" << endl;
	c5.printRecord();
	c6.printRecord();
	cout << endl;

	Complex c7( 100, 400 );
	Complex c8( 10, 20 );
	cout << "c7	:	";
	c7.printRecord();
	cout << "c8	:	";
	c8.printRecord();
	cout << endl;

	c7 /= c8;	//operator/=( c7, c8 );
	cout << "c7+=c8	:	" << endl;
	c7.printRecord();
	c8.printRecord();
	cout << endl;

	Complex c9( 15, 25 );
	Complex c10( 2, 4 );
	cout << "c9	:	";
	c9.printRecord();
	cout << "c10	:	";
	c10.printRecord();
	cout << endl;

	c9 %= c10;	//operator%=( c9, c10 );
	cout << "c9%=c10	:	" << endl;
	c9.printRecord();
	c10.printRecord();
	cout << endl;
	return 0;
}

