#pragma pack(1)
#include<iostream>
using namespace std;
class Base
{
private:
	int num1;
	int num2;
public:
	Base( void )
	{
		this->num1 = 10;
		this->num2 = 20;
	}
	Base( int num1, int num2 )
	{
		this->num1 = num1;
		this->num2 = num2;
	}
	void showRecord( void )
	{
		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
	}
	void printRecord( void )
	{
		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
	}
};
class Derived : public Base
{
private:
	int num3;
public:
	Derived( void )
	{
		this->num3 = 30;
	}
	Derived( int num1, int num2, int num3 ) : Base( num1, num2 )
	{
		this->num3 = num3;
	}
	void printRecord( void )
	{
		Base::printRecord();
		cout<<"Num3	:	"<<this->num3<<endl;
	}
	void displayRecord( void )
	{
		Base::showRecord();
		cout<<"Num3	:	"<<this->num3<<endl;
	}
};

int main( void )
{
	Derived d;
	//Derived d(10,20);
	d.printRecord();
	d.showRecord();
	cout << "Size	:	" << sizeof(d);
	return 0;
}

/*int main( void )
{
	//Base b;
	Base b(10,20);
	b.printRecord();
	b.showRecord();
	cout << "Size	:	" << sizeof(b);
	return 0;
}*/
