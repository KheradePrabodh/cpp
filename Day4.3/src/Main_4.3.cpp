#include<stdio.h>

/*
namespace na
{
	int num1 = 10;
}
namespace na
{
	int num2 = 20;
}
int main( void )
{
	printf("Num1	:	%d\n", na::num1);	//10
	printf("Num2	:	%d\n", na::num2);	//20
	return 0;
}*/



/*
namespace na
{
	int num1 = 10;
	int num3 = 30;
}
namespace na
{
	int num2 = 20;
}
int main( void )
{
	printf("Num1	:	%d\n", na::num1);	//10
	printf("Num3	:	%d\n", na::num3);	//30

	printf("Num2	:	%d\n", na::num2);	//20
	return 0;
}*/


namespace na
{
	int num1 = 10;
	int num3 = 30;
}
namespace na
{
	int num2 = 20;
	//int num3 = 40; //redefinition of ‘int na::num3’
}
int main( void )
{
	printf("Num1	:	%d\n", na::num1);
	printf("Num3	:	%d\n", na::num3);

	printf("Num2	:	%d\n", na::num2);
	return 0;
}


