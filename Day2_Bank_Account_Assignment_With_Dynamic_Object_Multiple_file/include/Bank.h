#ifndef BANK_H_
#define BANK_H_

struct Bank
{
	int index;
	struct Account *arr[ 5 ];
};

void accept_account_info( struct Account * );
int create_account( struct Bank *, struct Account * );
float deposit( struct Bank *, int , float  );
float withdraw( struct Bank *, int , float );
struct Account* get_account_details( struct Bank *, int );
void print_account_info( struct Account * );
void accept_account_number( int * );
void accept_amount( float * );
void print_account_number( int );
void print_balance( float );

#endif /* BANK_H_ */
