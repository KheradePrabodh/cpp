#include<iostream>

#include "../include/Account.h"
#include "../include/Bank.h"

int count = 1000;

void accept_account_info( struct Account *ptrAccount )
{
	printf("Name	:	");
	scanf("%s", ptrAccount->name );
	printf("Type	:	");
	scanf("%s", ptrAccount->type );
	printf("Balance	:	");
	scanf("%f", &ptrAccount->balance );
}

int create_account( struct Bank *ptrBank, struct Account *ptr )
{
	ptr->number = ++ count;
	ptrBank->arr[ ++ ptrBank->index ] = ptr;
	return ptr->number;
}

float deposit( struct Bank *ptrBank, int number, float amount )
{
	for( int index = 0; index <= ptrBank->index; ++ index )
	{
		if( ptrBank->arr[ index ]->number == number  )
		{
			ptrBank->arr[ index ]->balance += amount;
			return ptrBank->arr[ index ]->balance;
		}
	}
	return 0;
}

float withdraw( struct Bank *ptrBank, int number, float amount )
{
	for( int index = 0; index <= ptrBank->index; ++ index )
	{
		if( ptrBank->arr[ index ]->number == number  )
		{
			ptrBank->arr[ index ]->balance -= amount;
			return ptrBank->arr[ index ]->balance;
		}
	}
	return 0;
}

struct Account* get_account_details( struct Bank *ptrBank, int number )
{
       struct Account *account;
	for( int index = 0; index <= ptrBank->index; ++ index )
	{
		if( ptrBank->arr[ index ]->number == number  )
		{
			account = ptrBank->arr[ index ];
		}
	}
	return account;
}

void print_account_info( struct Account *ptrAccount )
{
	printf("%-20s%-5d%-10s%-10.2f\n", ptrAccount->name, ptrAccount->number, ptrAccount->type, ptrAccount->balance);
}

void accept_account_number( int *number )
{
	printf("Account number	:	");
	scanf("%d", number );
}

void accept_amount( float *amount )
{
	printf("Amount	:	");
	scanf("%f", amount );
}

void print_account_number( int number )
{
	printf("Account number	:	%d\n", number);
}

void print_balance( float balance )
{
	printf("Balance	:	%f\n", balance );
}
