#include<iostream>


#include "../include/Account.h"
#include "../include/Bank.h"

int menu_list( void )
{
	int choice;
	printf("0.Exit\n");
	printf("1.Create New Account\n");
	printf("2.Deposit\n");
	printf("3.Withdraw\n");
	printf("4.Print Account details\n");
	printf("Enter choice	:	");
	scanf("%d",&choice);
	return choice;
}

int main( void )
{
	int choice, accNumber;
	float balance, amount;
	struct Bank *ptrBank=(struct Bank*) malloc(sizeof(struct Bank));
	ptrBank->index=  -1 ;
	struct Account *ptrAccount=(struct Account*) malloc(sizeof(struct Account));
	while( ( choice = menu_list( ) ) )
	{
		switch( choice )
		{
		case 1:
			accept_account_info(ptrAccount);
			accNumber = create_account(ptrBank, ptrAccount);
			print_account_number(accNumber);
			break;
		case 2:
			accept_account_number(&accNumber);
			accept_amount( &amount );
			balance = deposit(ptrBank, accNumber, amount );
			print_balance(balance);
			break;
		case 3:
			accept_account_number(&accNumber);
			accept_amount(&amount );
			balance = withdraw(ptrBank, accNumber, amount );
			print_balance(balance);
			break;
		case 4:
			accept_account_number(&accNumber);
			ptrAccount = get_account_details(ptrBank, accNumber);
			print_account_info(ptrAccount);
			break;
		}
	}
	free(ptrBank);
	ptrBank=NULL;
	free(ptrAccount);
	ptrAccount=NULL;
	return 0;
}
