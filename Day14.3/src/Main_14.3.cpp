#include<cstring>
#include<iostream>
using namespace std;

class Array
{
private:
	int size;	//4 bytes
	int *arr;	//2/4/8 bytes
public:
	//Array *const this = &a1
	Array( void ) : size( 0 ), arr( NULL )
	{	}

	//Array *const this = &a1
	Array( int size )
	{
		this->size = size;
		this->arr = new int[ size ];
	}

	//Array *const this = &a1
	void acceptRecord( void )
	{
		for( int index = 0; index < this->size; ++ index )
		{
			cout<<"Enter element	:	";
			cin>>this->arr[ index ];
		}
	}

	//Array *const this = &a1
	void printRecord( void )
	{
		for( int index = 0; index < this->size; ++ index )
			cout<<this->arr[ index ]<<endl;
	}

	//Array *const this = &a1
	/*void clear( void )
	{
		if( this->arr != NULL )
		{
			delete[] this->arr;
			this->arr = NULL;
		}
	}*/
};
int main( void )
{
	Array a1(5);
	a1.acceptRecord();
	a1.printRecord();
	/*a1.clear( );
	a1.clear();
	*/return 0;
}
