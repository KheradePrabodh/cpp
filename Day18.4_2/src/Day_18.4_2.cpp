#include<iostream>
using namespace std;
class Point
{
private:
	int xPos;
	int yPos;
public:
	Point( int xPos = 0, int yPos = 0 )
	{
		this->xPos = xPos;
		this->yPos = yPos;
	}

	Point operator+=( Point &other )
	{
		this->xPos -= other.xPos;
		this->yPos -= other.yPos;
		return ( *this );
	}
	Point operator-=( Point &other )
	{
		this->xPos -= other.xPos;
		this->yPos -= other.yPos;
		return ( *this );
	}
	Point operator*=( Point &other )
	{
		this->xPos *= other.xPos;
		this->yPos *= other.yPos;
		return ( *this );
	}
	Point operator/=( Point &other )
	{
		this->xPos /= other.xPos;
		this->yPos /= other.yPos;
		return ( *this );
	}
	Point operator%=( Point &other )
	{
		this->xPos %= other.xPos;
		this->yPos %= other.yPos;
		return ( *this );
	}
	void printRecord( void )
	{
		cout<<this->xPos<<"	"<<this->yPos<<endl;
	}
};
int main( void )
{
	Point pt1(30,40);
	Point pt2(10,20);
	cout<<"pt1	:	";
	pt1.printRecord();
	cout<<"pt2	:	";
	pt2.printRecord();
	cout<<endl;


	Point pt3(30,40);
	Point pt4(10,20);
	pt3 -= pt4;	//pt1.operator-=( pt2 )
	cout << "pt3-=pt4	:	" << endl;
	pt3.printRecord();
	pt4.printRecord();
	cout<<endl;

	Point pt5(30,40);
	Point pt6(10,20);
	pt5 *= pt6;	//pt1.operator*=( pt2 )
	cout << "pt5*=pt6	:	" << endl;
	pt5.printRecord();
	pt6.printRecord();
	cout<<endl;

	Point pt7(30,40);
	Point pt8(10,20);
	pt7 /= pt8;	//pt1.operator/=( pt2 )
	cout << "pt7/=pt8	:	" << endl;
	pt7.printRecord();
	pt8.printRecord();
	cout<<endl;

	Point pt9(30,40);
	Point pt10(10,20);
	pt9 %= pt9;	//pt1.operator%=( pt2 )
	cout << "pt9%=pt10	:	" << endl;
	pt9.printRecord();
	pt10.printRecord();
	cout<<endl;

	return 0;
}
