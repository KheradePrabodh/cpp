#include <iostream>

class Employee
{
//private: by default
	char name[50];
	int empid;
	float salary;
public:
	void acceptRecord()
	{
		printf("Name	:	");
		scanf("%[^\n]%*c", name);
		printf("Empid	:	");
		scanf("%d", &empid);
		printf("Salary	:	");
		scanf("%f", &salary);
	}

	void printRecord()
	{
		printf("Name	:	%s\n", name);
		printf("Empid	:	%d\n", empid);
		printf("Salary	:	%.2f\n", salary);
	}
};

int main()
{
	Employee emp;
	emp.acceptRecord();
	emp.printRecord();
}
