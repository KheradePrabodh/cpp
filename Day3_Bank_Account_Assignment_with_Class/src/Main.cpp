#include<stdio.h>
#include<string.h>
#include<stdlib.h>

class Account
{
public:
	char name[ 50 ];
	int number;
	char type[ 50 ];
	float balance;
};

int count = 1000;

class Bank
{
private:
	int index;
	Account arr[ 5 ];
public:
	void init()
	{
		index=-1;
	}
	void accept_account_info( Account *ptraccount )
	{
		printf("Name	:	");
		scanf("%s", ptraccount->name );
		printf("Type	:	");
		scanf("%s", ptraccount->type );
		printf("Balance	:	");
		scanf("%f", &ptraccount->balance );
	}

	int create_account(   Account account )
	{
		account.number = ++ count;
		arr[ ++index ] = account;
		return account.number;
	}

	float deposit( int number, float amount )
	{
		for( int i = 0; i <= index; ++i )
		{
			if( arr[ i ].number == number  )
			{
				arr[ i ].balance += amount;
				return arr[ i ].balance;
			}
		}
		return 0;
	}

	float withdraw( int number, float amount )
	{
		for( int i = 0; i <= index; ++ i )
		{
			if( arr[ i ].number == number  )
			{
				arr[ i ].balance -= amount;
				return arr[ i ].balance;
			}
		}
		return 0;
	}

	Account get_account_details( int number )
	{
		Account account;
		for( int i = 0; i <= index; ++ i )
		{
			if( arr[ index ].number == number  )
			{
				account = arr[ index ];
			}
		}
		return account;
	}

	void print_account_info( Account *ptraccount )
	{
		printf("%-20s%-5d%-10s%-10.2f\n", ptraccount->name, ptraccount->number, ptraccount->type, ptraccount->balance);
	}

	void accept_account_number( int *number )
	{
		printf("Account number	:	");
		scanf("%d", number );
	}

	void accept_amount( float *amount )
	{
		printf("Amount	:	");
		scanf("%f", amount );
	}

	void print_account_number( int number )
	{
		printf("Account number	:	%d\n", number);
	}

	void print_balance( float balance )
	{
		printf("Balance	:	%f\n", balance );
	}
};
int menu_list( void )
{
	int choice;
	printf("0.Exit\n");
	printf("1.Create New Account\n");
	printf("2.Deposit\n");
	printf("3.Withdraw\n");
	printf("4.Print Account details\n");
	printf("Enter choice	:	");
	scanf("%d",&choice);
	return choice;
}

int main( void )
{
	int choice, accNumber;
	float balance, amount;
	Bank bank ;
	bank.init();
	Account account;
	while( ( choice = menu_list( ) ) )
	{
		switch( choice )
		{
		case 1:
			bank.accept_account_info(&account);
			accNumber = bank.create_account( account);
			bank.print_account_number(accNumber);
			break;
		case 2:
			bank.accept_account_number(&accNumber);
			bank.accept_amount( &amount );
			balance = bank.deposit( accNumber, amount );
			bank.print_balance(balance);
			break;
		case 3:
			bank.accept_account_number(&accNumber);
			bank.accept_amount(&amount );
			balance = bank.withdraw( accNumber, amount );
			bank.print_balance(balance);
			break;
		case 4:
			bank.accept_account_number(&accNumber);
			account = bank.get_account_details( accNumber);
			bank.print_account_info(&account);
			break;
		}
	}
	return 0;
}
