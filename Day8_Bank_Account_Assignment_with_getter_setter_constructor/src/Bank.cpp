#include "../include/Bank.h"
using namespace bank;

int count = 1000;

Bank :: Bank(void)
{
	this->index=-1;
};

void Bank :: accept_account_info( Account *ptraccount )
{
	string name,type;
	float balance;
	cout << endl;
	cout << "Name	:	";
	cin >> name ;
	ptraccount->setName(name);
	cout << "Type	:	";
	cin >>type;
	ptraccount->setType(type);
	cout << "Balance	:	";
	cin>>balance;
	ptraccount->setBalance(balance);
}

int Bank :: create_account(  Account account )
{
	account.setNumber(++ count);
	this->arr[++this->index] = account;
	return account.getNumber();
}

bool Bank:: checkAccountNumber(int *accNumber)
{
	int flag=0;
	for(int index=0;index<=this->index;index++)
	{
		if( this->arr[index].getNumber() == *accNumber  )
			flag=1;
		else
			flag=0;
	}
	if (flag==1)
		return true;
	else
		return false;
}

float Bank :: deposit( int number, float amount )
{
	for( int index = 0; index <= this->index; ++ index )
	{
		if( this->arr[index].getNumber() == number  )
		{
			this->arr[index].setBalance(this->arr[index].getBalance()+amount);
			return this->arr[index].getBalance();
		}
	}
	return 0;
}

float Bank :: withdraw( int number, float amount )
{
	for( int index = 0; index <= this->index; ++ index )
	{
		if( this->arr[index].getNumber() == number  )
		{
			if(amount<=this->arr[index].getBalance())
			{
				this->arr[index].setBalance(this->arr[index].getBalance()-amount);
				return this->arr[index].getBalance();
			}
			else
			{
				cout << "Not Enough Balance" << endl;
				return this->arr[index].getBalance();
			}
		}
	}
	return 0;
}

Account Bank :: get_account_details( int number )
{
	Account account;
	for( int index = 0; index <= this->index; ++ index )
	{
		if( this->arr[index].getNumber() == number  )
		account = this->arr[index];
	}
	return account;
}

void Bank :: print_account_info( Account *ptraccount )
{
	cout << endl;
	cout << "Account Holder Name	:	" << ptraccount->getName() << endl;
	cout << "Account Number		:	" << ptraccount->getNumber() << endl;
	cout << "Account Type		:	" << ptraccount->getType()<< endl;
	cout << "Account Balance		:	" << ptraccount->getBalance() << endl;
	cout << endl;
}

void Bank :: accept_account_number( int *number )
{
	cout << "Account number	:	";
	cin >> *number ;
}

void Bank :: accept_amount( float *amount )
{
	cout << "Amount	:	";
	cin >> *amount ;
}

void Bank :: print_account_number( int number )
{
	cout << "Account number	:	" << number << endl;
}

void Bank :: print_balance( float balance )
{
	cout << "Available Balance	:	"<< balance << endl;
}
