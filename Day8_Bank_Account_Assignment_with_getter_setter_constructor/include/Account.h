#ifndef ACCOUNT_H_
#define ACCOUNT_H_

#include<iostream>
using namespace std;

namespace account
{
	class Account
	{
	private:
		string name;
		int number;
		string type;
		float balance;
	public:
		Account(void);
		string getName();
		void setName(string name);
		int getNumber();
		void setNumber(int number);
		float getBalance();
		void setBalance(float balance);
		string getType();
		void setType(string type);
	};
}

#endif /* ACCOUNT_H_ */
