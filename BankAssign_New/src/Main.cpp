#include<iostream>
#include<string>
#include<stdlib.h>
#include<ctime>

using namespace std;

namespace BANK{

class Transaction
{
private:
	string date;
	int acc_no;
	string type;
	float amount;
public:
	int getAccNo() const {
		return acc_no;
	}

	void setAccNo(int accNo) {
		acc_no = accNo;
	}

	float getAmount() const {
		return amount;
	}

	void setAmount(float amount) {
		this->amount = amount;
	}

	const string& getDate() const {
		return date;
	}

	void setDate(const string &date) {
		this->date = date;
	}

	const string& getType() const {
		return type;
	}

	void setType(const string type) {
		this->type = type;
	}

	void printDetails(){
		cout<<this->date<<this->acc_no<<"	"<<this->type<<"	"<<this->amount<<endl;;
	}
};
class Exception
{
	string message;
public:
	Exception (const string message)throw():message(message)
{}

	const string& getMessage() const {
		return message;
	}
};

class Account
{
	string name;
	int number;
	string type ;
	float balance;
	Transaction tArr[20];
	int transIndex;

public:
	int getTransIndex() const
	{
		return transIndex;
	}
	void setTransIndex(int transIndex)
	{
		this->transIndex = transIndex;
	}
	void setAccName(string Name)
	{
		this->name = Name;
	}
	void setAccType(string Type)
	{
		this->type = Type;
	}
	void setAccNumber(int number)
	{
		this->number = number;
	}
	void setAccBalance(float balance)
	{
		this->balance = balance;
	}
	string getAccName()
	{
		return name;
	}
	string getAccType()
	{
		return type;
	}
	int getAccNumber()
	{
		return number;
	}
	float getAccBalance()
	{
		return balance;
	}
	void addAccBalance(float balance)
	{
		this->balance += balance;
	}
	void subAccBalance(float balance)throw(Exception)
											{
		if(balance > this->balance)
			throw Exception("Can't Withdraw the amount");
		this->balance -= balance;
											}
	void printAccountInfo()
	{
		cout<<"Name		:	"<<this->name<<endl;
		cout<<"Account Number	:	"<<this->number<<endl;
		cout<<"Account Type	:	"<<this->type<<endl;
		cout<<"Balance		:	"<<this->balance<<endl;;
	}
	void setTransDetails(int amount, string type)
	{
		time_t my_time = time(NULL);
		this->transIndex+=1;
		this->tArr[this->transIndex].setAccNo(this->number);
		this->tArr[this->transIndex].setType(type);
		this->tArr[this->transIndex].setAmount(amount);
		this->tArr[this->transIndex].setDate(ctime(&my_time));
	}
	void getTransDetails()
	{
		for( int i = 0; i <= this->transIndex; ++ i )
		{
			this->tArr[i].printDetails();
		}
	}

};

int count = 1000;

class Bank
{
private:
	int index;
	int size;
	Account *arr;

public:
	Bank():index(-1),size(0),arr(NULL)
{ }
	Bank(int size):index(-1),size(size)
	{
		this->arr = new Account[sizeof(Account)*size];
	}
	int getBankIndex()
	{
		return this->index;
	}
	void setBankIndex(int i)
	{
		this->index=i;
	}
	int create_account()throw(Exception)
	{
		if(this->index < (this->size-1))
		{
			++this->index;
			string name,type;
			float balance;
			cout<<"Name	:	";
			cin>>name;
			this->arr[this->index].setAccName(name);
			cout<<"Type	:	";
			cin>>type;
			this->arr[this->index].setAccType(type);
			cout<<"Balance	:	";
			cin>>balance;
			this->arr[this->index].setAccBalance(balance);
			this->arr[this->index].setTransIndex(-1);
			this->arr[this->index].setAccNumber(++count);

			return this->arr[this->index].getAccNumber();
		}
		else
			throw Exception("Sorry Can't Create Account");
	}

	float deposit(int number, float amount )throw(Exception)
	{
		for( int i = 0; i <= this->index; ++ i)
		{
			if( this->arr[ i ].getAccNumber() == number  )
			{
				this->arr[ i ].addAccBalance(amount);
				this->arr[ i ].setTransDetails(amount,"Deposit");
				return this->arr[ index ].getAccBalance();
			}
		}
		throw Exception("Invalid Account Number");
		return 0;
	}

	float withdraw(int number, float amount )throw(Exception)
											{
		for( int i = 0; i <= index; ++ i )
		{
			if( this->arr[ i ].getAccNumber() == number  )
			{
				this->arr[ i ].subAccBalance(amount);
				this->arr[ i ].setTransDetails(amount,"Withdraw");
				return this->arr[ i ].getAccBalance();
			}
		}
		throw Exception("Invalid Account Number");
		return 0;
											}

	void get_account_details(int number )throw(Exception)
	{
		for( int i = 0; i <= this->index; ++ i )
		{
			if( this->arr[ i ].getAccNumber() == number  )
			{
				this->arr[ i ].printAccountInfo();
				return;
			}
		}
		throw Exception("Invalid Account Number");
	}
	void getTransDetails(int number)throw(Exception)
	{
		for( int i = 0; i <= this->index; ++ i )
		{
			if( this->arr[i].getAccNumber() == number  )
			{
				this->arr[i].getTransDetails();
				return;
			}
		}
		throw Exception("Invalid Account Number");
	}
	~Bank()
	{
		delete[] this->arr;
	}

};


void accept_account_number( int *number )
{
	cout<<"Account number	:	";
	cin>>*number;
}
void accept_amount( float *amount )
{
	cout<<"Amount	:	";
	cin>>*amount;
}
void print_account_number(int number )
{
	cout<<"Account number	:	"<<number<<endl;
}
void print_balance( float balance )
{
	cout<<"Balance	:	"<<balance<<endl;
}

int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Create New Account"<<endl;
	cout<<"2.Deposit"<<endl;
	cout<<"3.Withdraw"<<endl;
	cout<<"4.Print Account details"<<endl;
	cout<<"5.List Transaction"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
}

int main( void )
{
	using namespace BANK;
	int choice, accNumber,count;
	float balance, amount;
	cout<<"Enter No of accounts in Bank	:	";
	cin>>count;
	Bank *bank=new Bank(count);
	bank->setBankIndex(-1);
	while( ( choice = menu_list( ) ) != 0 )
	{
		try
		{
			switch( choice )
			{
			case 1:

				accNumber = bank->create_account();
				print_account_number(accNumber);
				break;
			case 2:
				accept_account_number(&accNumber);
				accept_amount( &amount );
				balance = bank->deposit(accNumber, amount );
				print_balance(balance);
				break;
			case 3:
				accept_account_number(&accNumber);
				accept_amount(&amount );
				balance = bank->withdraw(accNumber, amount );
				print_balance(balance);
				break;
			case 4:
				accept_account_number(&accNumber);
				bank->get_account_details(accNumber);
				break;
			case 5:
				accept_account_number(&accNumber);
				bank->getTransDetails(accNumber);
				break;
			}
		}
		catch(Exception &ex)
		{
			cout<<ex.getMessage()<<endl;
		}
		catch(...)
		{
			cout<<"Write catch function"<<endl;
		}
	}
	delete bank;
	return 0;
}
