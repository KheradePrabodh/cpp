#include<iostream>
using namespace std;

int choice();
int choiceForInsert();
int choiceForDelete();

class Exception
{
	string message;
public:
	Exception (const string message)throw():message(message)
{	}
	const string& getMessage() const
	{
		return message;
	}
};
class LinkedList
{
private:
	class Node
	{
		int data;
		Node *next;
	public:
		Node(int data = 0):data(data),next(NULL)
	{	}
		friend LinkedList;
	};
private:
	Node *head;
	Node *tail;
	int size;

private:
	void addFirst(int element)throw(bad_alloc)
	{
		Node *ptr = new Node(element);
		if(this->isempty())
			this->head = this->tail = ptr;
		else if(this->head == this->tail)
			ptr->next = this->tail;
		else
			ptr->next = this->head;

		this->head = ptr;
		++this->size;
	}
	void addAtPosition(int element,int position)throw(bad_alloc,Exception)
	{
		Node *ptr = new Node(element);
		if(position == 1)
			this->addFirst(element);
		else if(position == (this->size+1))
			this->addLast(element);
		else if( 1 < position && position <= this->size )
		{
			int count = 1;
			Node *trav = this->head;
			while(count < ( position -1 ) )
			{
				trav = trav->next;
				++count;
			}
			ptr->next = trav->next;
			trav->next = ptr;
		}
		else
			throw Exception("Invalid Position");

		++this->size;

	}
	void addLast(int element)throw(bad_alloc)
	{
		Node *ptr = new Node(element);
		if(this->isempty())
			this->head = this->tail = ptr;
		else if(this->head == this->tail)
			this->head->next = ptr;
		else
			this->tail->next = ptr;
		this->tail = ptr;
		++this->size;
	}
	void delFirst()throw(Exception)
	{
		if(this->isempty())
			throw Exception("Linked List is Empty");
		else if(this->head == this->tail)
		{
			delete this->head;
			this->head = this->tail = NULL;
		}
		else
		{
			Node *ptr = this->head;
			this->head = ptr->next;
			delete ptr;
		}
		--this->size;
	}
	void delAtPosition(int position)throw(Exception)
	{
		if(position == 1)
			this->delFirst();
		else if(position == (this->size+1) )
			this->delLast();
		else if( 1 < position && position <= this->size )
		{
			int count = 1;
			Node *trav = this->head;
			while(count < ( position -1 ) )
			{
				trav = trav->next;
				++count;
			}
			Node *temp = trav->next;
			trav->next = trav->next->next;
			delete temp;
		}
		else
			throw Exception("Invalid Position");
		--this->size;
	}
	void delLast()throw(Exception)
	{
		if(this->isempty())
			throw Exception("Linked List is Empty");
		else if(this->head == this->tail)
		{
			delete this->tail;
			this->head = this->tail = NULL;
		}
		else
		{
			Node *ptr = this->head;
			while(ptr->next != this->tail)
				ptr = ptr->next;
			ptr->next = NULL;
			delete this->tail;
			this->tail = ptr;
		}
		--this->size;
	}
public:
	LinkedList():head(NULL),tail(NULL),size(0)
{	}
	bool isempty()
	{
		if(this->head == NULL)
			return true;
		return false;
	}
	void addElement(int element)throw(bad_alloc,Exception)
	{
		int ch = choiceForInsert();
		int position;
		switch(ch)
		{
		case 1:
			this->addFirst(element);
			break;
		case 2:
			cout<<"Enter Position	:	";
			cin>>position;
			this->addAtPosition(element,position);
			break;
		case 3:
			this->addLast(element);
		}
	}
	void delElement()throw(Exception)
	{
		int ch = choiceForDelete();
		int position;
		switch(ch)
		{
		case 1:
			this->delFirst();
			break;
		case 2:
			cout<<"Enter Position	:	";
			cin>>position;
			this->delAtPosition(position);
			break;
		case 3:
			this->delLast();
			break;
		}
	}
	void print( void )throw( Exception )
	{
		if( this->isempty())
			throw Exception("Linked List is Empty");
		Node *trav = this->head;
		while( trav != NULL )
		{
			cout<<trav->data<<"	";
			trav = trav->next;
		}
		cout<<endl;
	}
	~LinkedList()
	{
		while(!this->isempty())
		{
			this->delFirst();
		}
	}
};

int main( void )
{
	int ch,element;
	LinkedList list1;
	while((ch = choice() ) != 0 )
	{
		try
		{
			switch(ch)
			{
			case 1:
				printf("Enter element to add	:	");
				scanf("%d",&element);
				list1.addElement(element);
				break;
			case 2:
				list1.delElement();
				break;
			case 3:
				list1.print();
				break;
			}
		}
		catch(Exception &ex)
		{
			cout<<ex.getMessage()<<endl;
		}
		catch(bad_alloc &ex)
		{
			cout<<ex.what()<<endl;
		}
		catch(...)
		{
			cout<<"This is Generic catch Block"<<endl;
		}
	}

	return 0;
}
int choice()
{
	int choice;
	cout<<endl<<"0.EXIT"<<endl;
	cout<<"1.Add Element"<<endl;
	cout<<"2.Delete Element"<<endl;
	cout<<"3.Show All Element"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;

}
int choiceForInsert()
{
	int choice;
	cout<<"1.Add Element at Start	";
	cout<<"2.Add Element at Position	";
	cout<<"3.Add Element at End"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;

}
int choiceForDelete()
{
	int choice;
	cout<<"1.Delete Element at Start	";
	cout<<"2.Delete Element at Position	";
	cout<<"3.Delete Element at End"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;

}
