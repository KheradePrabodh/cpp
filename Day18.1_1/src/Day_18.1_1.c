#include<stdio.h>

struct Point
{
	int xPos;
	int yPos;
};

int main( void )
{
	struct Point pt1 = { 10, 20 };
	struct Point pt2 = { 30, 40 };
	struct Point pt3;

	//pt3 = pt1 + pt2;	//Not OK	//	error: invalid operands to binary + (have ‘struct Point’ and ‘struct Point’)

	pt3.xPos = pt1.xPos + pt2.xPos;	//	OK
	pt3.yPos = pt1.yPos + pt2.yPos;	//	OK

	return 0;
}

/*int main( void )
{
	int num1 = 10;
	int num2 = 20;
	int result = num1 + num2;
	//cout<<"Result	:	"<<result<<endl;
	printf("Result	:	%d\n", result);
	return 0;
}*/
