#include<stdio.h>

/*
namespace na
{
	int num1 = 10;
}
int main( void )
{
	using namespace na;
	printf("Num1	:	%d\n",num1); //ok //10

	return 0;
}*/

/*namespace na
{
	int num1 = 10;
}
int main( void )
{
	int num1 = 20;
	using namespace na; //not usable when we want to access namespace variable
						//if local and namespace variable have same name
						// local is preferred.
	printf("Num1	:	%d\n",num1); // ok //20 local preference
	printf("Num1	:	%d\n",na::num1); // ok //10
	return 0;
}*/

namespace na
{
	int num1 = 10;
}
int num1 = 20;
int main( void )
{
	using namespace na;
	printf("Num1	:	%d\n",num1);//ambiguity error

	return 0;
}

