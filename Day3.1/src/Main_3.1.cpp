#include<stdio.h>

struct Employee
{
	char name[30];
	int empid;
	float salary;
};

void acceptRecord(struct Employee *emp)
{
	printf("Enter Your Name	:	");
	scanf("%[^\n]%*c",emp->name);
	printf("Enter Your Empid	:	");
	scanf("%d",&emp->empid);
	printf("Enter Your Salary	:	");
	scanf("%f",&emp->salary);
}

void printRecord(struct Employee *emp)
{
	printf("Name	:	%s\n",emp->name);
	printf("Empid	:	%d\n",emp->empid);
	printf("Salary	:	%.2f\n",emp->salary);
}

int main(void)
{
	struct Employee emp;
	acceptRecord(&emp);
	printRecord(&emp);
	return 0;
}
