#include<iostream>
using namespace std;
class Point
{
private:
	int xPos;
	int yPos;
public:
	Point( int xPos = 0, int yPos = 0 )
	{
		this->xPos = xPos;
		this->yPos = yPos;
	}

	Point operator+( int value )
	{
		Point temp;
		temp.xPos = this->xPos + value;
		temp.yPos = this->yPos + value;
		return temp;
	}
	void printRecord( void )
	{
		cout<<this->xPos<<"	"<<this->yPos<<endl;
	}
};
int main( void )
{
	Point pt1(10,20);
	Point pt2(30,40);

	pt2 = pt1 + 5;	//pt2 = pt1.operator+( 5 );
	cout << "pt1	:	";
	pt1.printRecord();
	cout << "pt2	:	";
	pt2.printRecord();
	return 0;
}
