#include"../include/Part.h"
using namespace kd1;

namespace kd1
{
Part:: Part(string desc, double rate):desc(desc),rate(rate)
{	}
const string& Part:: getDesc() const {
	return desc;
}

void Part:: setDesc(const string &desc) {
	this->desc = desc;
}

double Part:: getRate() const {
	return rate;
}

void Part:: setRate(double rate) {
	this->rate = rate;
}
void Part:: input(const string &desc , double rate)
{
	this->desc = desc;
	this->rate = rate;
}
ostream& operator<<(ostream &cout , Part &other)
{
	cout<<"Part Name	:	"<<other.desc<<endl;
	cout<<"Part	Rate	:	"<<other.rate<<endl;
	return cout;
}
istream& operator>>(istream &cin , Part &other)
{
	cout<<"Part Name	:	";
	cin>>other.desc;
	cout<<"Part	Rate	:	";
	cin>>other.rate;
	return cin;
}
}
