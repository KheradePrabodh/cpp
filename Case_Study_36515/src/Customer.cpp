#include"../include/Customer.h"
using namespace kd1;

namespace kd1
{
Customer:: Customer(const string address , const string mobile , const string name):
														address(address),mobile(mobile),name(name)
{	}
const string& Customer:: getAddress() const {
	return address;
}

void Customer:: setAddress(const string &address) {
	this->address = address;
}

const string& Customer:: getMobile() const {
	return mobile;
}

void Customer:: setMobile(const string &mobile) {
	this->mobile = mobile;
}

const string& Customer:: getName() const {
	return name;
}

void Customer:: setName(const string &name) {
	this->name = name;
}

const vector<Vehicle>& Customer:: getVehList() const {
	return veh_list;
}

void Customer:: addVehicle(const Vehicle &veh) {
	veh_list.push_back(veh);
}
void Customer:: displayVehicle(void)
{
	for(size_t i=0 ; i < this->veh_list.size();i++)
		cout<<this->veh_list[i];
}
void Customer:: loadCustomer(string& line)
{
	stringstream str(line);
	string tok[3];
	for( int i = 0 ; i < 3 ; i++ )
		getline(str,tok[i],',');
	this->name = tok[0];
	this->mobile = tok[1];
	this->address = tok[2];
	ifstream fin("csv/Vehicle.csv");
	if(!fin)
	{
		cout<<"failed to open Vehicle.csv file"<<endl;
		return;
	}
	string line2;
	while(getline(fin,line2))
	{
		stringstream str(line2);
		string tok[4];
		for( int i = 0 ; i < 4 ; i++ )
			getline(str,tok[i],',');
		if(this->mobile == tok[0])
		{
			Vehicle add(tok[1],tok[2],tok[3]);
			this->veh_list.push_back(add);
		}

	}
	fin.close();

}
void Customer:: storeCustomer(ofstream& fout)
{
	fout<<this->getName()<<","<<this->getMobile()<<","<<this->getAddress()<<endl;

	ofstream fout2("csv/Vehicle.csv",ios::app);
	if(!fout2)
	{
		cout<<"Failed to open Vehicle.csv file";
		return;
	}
	vector<Vehicle>::iterator itr = this->veh_list.begin();
	while(itr != this->veh_list.end())
	{
		fout2<<this->mobile<<","<<itr->getCompany()<<","<<itr->getModel()<<","<<itr->getNumber()<<endl;
		itr++;
	}
	fout2.close();
}
ostream& operator<<(ostream &cout , Customer &other)
{
	cout<<other.name<<"	"<<other.mobile<<"	"<<other.address<<endl;
	cout<<"Vehicle list	"<<endl;
	other.displayVehicle();
	return cout;
}
istream& operator>>(istream &cin , Customer &other)
{
	Vehicle v1;
	cout<<"Enter Name	:	";
	cin>>other.name;
	cout<<"Enter Mobile No	:	";
	cin>>other.mobile;
	cout<<"Enter Address	:	";
	cin>>other.address;
	cout<<"Enter Vehicle Details"<<endl;
	cin>>v1;
	other.addVehicle(v1);
	return cin;
}
}
