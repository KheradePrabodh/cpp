#include"../include/Maintenance.h"
using namespace kd1;

namespace kd1
{
Maintenance:: Maintenance( const string desc, double labourCharges):
										Service(desc),laborCharges(labourCharges)
{	}
void Maintenance:: addPart(const Part& newPart)
{
	partList.push_back(newPart);
}
double Maintenance:: price()
{
	return 0.0;
}

double Maintenance:: getLabourCharges() const {
	return laborCharges;
}

void Maintenance:: setLabourCharges(double laborCharges) {
	this->laborCharges = laborCharges;
}

const list<Part>& Maintenance:: getPartList() const {
	return partList;
}

void Maintenance:: input(void)
{
	cin>>*this;
}
void Maintenance:: display(void)
{
	cout<<*this;
}
ostream& operator<<(ostream &cout , Maintenance &other)
{
	cout<<other.getDesc()<<"	"<<endl;
	list<Part>::iterator itr = other.partList.begin();
	while(itr != other.partList.end())
	{
		cout<<*itr;
		itr++;
	}
	return cout;
}
istream& operator>>(istream &cin , Maintenance &other)
{
	string desc;
	cout<<"Enter Maintenance Name	:	";
	cin.ignore();
	getline(cin,desc);
	other.setDesc(desc);
	int choice;
	while(choice != 0)
	{
		cout<<"0.Exit"<<endl;
		cout<<"1. Add new part"<<endl;
		cin>>choice;
		switch(choice)
		{
		case 1:
			cout<<"Enter part details	:	"<<endl;
			Part temp;
			cin>>temp;
			other.partList.push_back(temp);
			break;
		}
	}
	cout<<"Enter Labor Charges	:	";
	cin>>other.laborCharges;
	return cin;
}
}
