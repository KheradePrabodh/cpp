#include"../include/Oil.h"
using namespace kd1;

namespace kd1
{

Oil::Oil(double cost, string desc):Service(desc),cost(cost)
{	}
double Oil:: getCost() const {
	return cost;
}

void Oil:: setCost(double cost) {
	this->cost = cost;
}
void Oil:: input(double cost)
{
	this->cost = cost;
}
double Oil:: price(void)
{
	return this->cost;
}
void Oil:: input(void)
{
	cin>>*this;
}
void Oil:: display(void)
{
	cout<<*this;
}

ostream& operator<<(ostream &cout , Oil &other)
{
	cout<<"Oil Name	:	"<<other.getDesc()<<endl;
	cout<<"Cost	:	"<<other.getCost()<<endl;


	return cout;
}
istream& operator>>(istream &cin , Oil &other)
{
	string desc;
	double cost;
	cout<<"Oil Name	:	";
	cin>>desc;
	other.setDesc(desc);
	cout<<"Cost	:	";
	cin>>cost;
	other.setCost(cost);
	return cin;
}
}
