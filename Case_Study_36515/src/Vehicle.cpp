#include"../include/Vehicle.h"
using namespace kd1;

namespace kd1
{

Vehicle::Vehicle( const string company , const string model , const string number ):
	company(company),model(model),number(number)
	{	}
const string& Vehicle:: getCompany() const {
	return company;
}

void Vehicle:: setCompany(const string &company) {
	this->company = company;
}

const string& Vehicle:: getModel() const {
	return model;
}

void Vehicle:: setModel(const string &model) {
	this->model = model;
}

const string& Vehicle:: getNumber() const {
	return number;
}

void Vehicle:: setNumber(const string &number) {
	this->number = number;
}
void Vehicle:: acceptRecord( void )
{
	cout<<"Enter Company Name	:	";
	cin>>this->company;
	cout<<"Enter Model Name	:	";
	cin>>this->model;
	cout<<"Enter Vehicle Number	:	";
	cin>>this->number;
}
ostream& operator<<(ostream &cout , Vehicle &other)
{
	cout<<other.company<<"	"<<other.model<<"	"<<other.number<<endl;
	return cout;
}
istream& operator>>(istream &cin , Vehicle &other)
{
	cout<<"Enter Company Name	:	";
	cin>>other.company;
	cout<<"Enter Model Name	:	";
	cin>>other.model;
	cout<<"Enter Vehicle Number	:	";
	cin>>other.number;
	return cin;
}
}


