#include"../include/Customer.h"
#include"../include/Oil.h"
#include"../include/Maintenance.h"

#include <string.h>
#include <fstream>
#include <sstream>

int Choice1(void);
int Choice2(void);
void loadCustomers(list<Customer>& cusList);
void saveCustomer(list<Customer>& cusList);
void showCustomerList(list<Customer>& cusList);
void addCustomer(list<Customer>& customerList);
Customer* getCustomer(list<Customer>& cusList , string name);

int main()
{
	using namespace kd1;
	int mychoice,serviceChoice;
	string name,number;
	Customer *newcust;
	list<Customer> myCustomerList;
	Service* myservice;
	loadCustomers(myCustomerList);
	while((mychoice = Choice1()) != 0)
	{
		switch(mychoice)
		{
		case 1:
			addCustomer(myCustomerList);
			break;
		case 2:
			cout<<"Customer List"<<endl;
			showCustomerList(myCustomerList);
			cout<<"Enter Customer name	:	";
			cin>>name;
			newcust = getCustomer(myCustomerList,name);
			while((serviceChoice=Choice2()) != 0 )
			{
				switch(serviceChoice)
				{
				case 1:
					myservice = new Oil;
					myservice->input();
					myservice->display();
					break;
				case 2:
					myservice = new Maintenance;
					myservice->input();
					myservice->display();
				}
				delete myservice;
			}
			break;
		case 3:
			break;

		}
	}
	saveCustomer(myCustomerList);
	return 0;
}



Customer* getCustomer(list<Customer>& cusList , string name)
{
	list<Customer>::iterator itr = cusList.begin();
	while(itr != cusList.end())
	{
		string temp = itr->getName();
		if(temp == name)
			return &*itr;
		itr++;
	}
	cout<<"Customer not registered"<<endl;
	return NULL;
}
void addCustomer(list<Customer>& customerList)
{
	Customer temp;
	cin>>temp;
	customerList.push_back(temp);
}
void showCustomerList(list<Customer>& customerList)
{
	list<Customer>::iterator itr = customerList.begin();
	while(itr != customerList.end())
	{
		cout<<itr->getName()<<'@'<<itr->getAddress()<<endl;
		itr++;
	}

}
int Choice1(void)
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Register New Customer"<<endl;
	cout<<"2.Servicing Request"<<endl;
	cout<<"3.Todays Business"<<endl;
	cout<<"Enter Choice	:	";
	cin>>choice;
	return choice;
}
int Choice2(void)
{
	int mychoice;
	cout<<"0.Servicing done"<<endl;
	cout<<"1.Oil Change"<<endl;
	cout<<"2.Maintenance / Repairing"<<endl;
	cout<<"Enter Choice	:	";
	cin>>mychoice;
	return mychoice;

}

void loadCustomers(list<Customer>& customerList)
{
	ifstream fin("csv/Customer.csv");
	if(!fin)
	{
		cout<<"failed to open customer.csv file"<<endl;
		return;
	}
	string line;
	while(getline(fin,line))
	{
		Customer mycust;
		mycust.loadCustomer(line);
		customerList.push_back(mycust);
	}
	fin.close();
}
void saveCustomer(list<Customer>& customerList)
{
	ofstream fout("csv/Customer.csv");
	if(!fout)
	{
		cout<<"Failed to open customer.csv file";
		return;
	}
	ofstream fout2("csv/Vehicle.csv");
	if(!fout2)
	{
		cout<<"Failed to open Vehicle.csv file";
		return;
	}
	fout2.close();
	list<Customer>::iterator itr = customerList.begin();
	while(itr != customerList.end())
	{
		itr->storeCustomer(fout);
		itr++;
	}
	fout.close();
}
