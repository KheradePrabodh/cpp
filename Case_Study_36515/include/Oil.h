#ifndef OIL_H_
#define OIL_H_
#include"../include/Service.h"
using namespace kd1;
namespace kd1
{
class Oil : public Service
{
private:
	double cost;
public:
	Oil(double cost  = 0.0 , string desc = "");
	double getCost() const;
	void setCost(double cost);
	void input(double cost);
	virtual double price(void);
	virtual void input(void);
	virtual void display(void);
	friend ostream& operator<<(ostream &cout , Oil &other);
	friend istream& operator>>(istream &cin , Oil &other);

};
}
#endif /* OIL_H_ */
