#ifndef SERVICE_H_
#define SERVICE_H_
#include<iostream>
#include<string>
using namespace std;
namespace kd1
{
class Service
{
private:
	string desc;
public:
	Service(string desc = " "):desc(desc)
{	}

	const string& getDesc() const {
		return desc;
	}
	void setDesc(const string &desc) {
		this->desc = desc;
	}
	virtual void input(void)
	{
		cout<<"Enter service Name	:	";
		cin>>this->desc;
	}
	virtual void display(void)
	{
		cout<<"Service name	:	"<<this->desc;
	}
	virtual double price()
	{
		return 0.0;
	}

};
}
#endif /* SERVICE_H_ */
