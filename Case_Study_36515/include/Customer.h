#ifndef CUSTOMER_H_
#define CUSTOMER_H_

#include"Vehicle.h"
#include<vector>
#include<list>
#include <fstream>
#include <sstream>
using namespace kd1;

namespace kd1
{
class Customer
{
private:
	string address;
	string mobile;
	string name;
	vector <Vehicle> veh_list;
public:
	Customer(const string address = "address", const string mobile = "mobile", const string name = "name");
	const string& getAddress() const;
	void setAddress(const string &address);
	const string& getMobile() const;
	void setMobile(const string &mobile);
	const string& getName() const;
	void setName(const string &name);
	const vector<Vehicle>& getVehList() const;
	void addVehicle(const Vehicle &veh);
	void displayVehicle(void);
	void loadCustomer(string& line);
	void storeCustomer(ofstream& fout);
	friend ostream& operator<<(ostream &cout , Customer &other);
	friend istream& operator>>(istream &cin , Customer &other);
};

}
#endif /* CUSTOMER_H_ */
