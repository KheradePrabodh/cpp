#ifndef PART_H_
#define PART_H_
#include<iostream>
#include<string>
using namespace std;

namespace kd1
{
class Part
{
private:
	string desc;
	double rate;
public:
	Part(string desc = " " , double rate = 0.0 );
	const string& getDesc() const;
	void setDesc(const string &desc);
	double getRate() const;
	void setRate(double rate);
	void input(const string &desc , double rate);
	friend ostream& operator<<(ostream &cout , Part &other);
	friend istream& operator>>(istream &cin , Part &other);
};
}
#endif /* PART_H_ */
