#ifndef VEHICLE_H_
#define VEHICLE_H_
#include<iostream>
#include<string>
using namespace std;
namespace kd1
{
class Vehicle
{
private:
	string company;
	string model;
	string number;
public:
	Vehicle( const string company = "company", const string model = "model", const string number = "number");
	const string& getCompany() const;
	void setCompany(const string &company) ;
	const string& getModel() const ;
	void setModel(const string &model);
	const string& getNumber() const ;
	void setNumber(const string &number);
	void acceptRecord( void );
	friend ostream& operator<<(ostream &cout , Vehicle &other);
	friend istream& operator>>(istream &cin , Vehicle &other);

};
}
#endif /* VEHICLE_H_ */
