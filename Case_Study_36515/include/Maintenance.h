#ifndef MAINTENANCE_H_
#define MAINTENANCE_H_
#include<list>
#include"../include/Part.h"
#include"../include/Service.h"
using namespace kd1;

namespace kd1
{
class Maintenance : public Service
{
private:
	double laborCharges;
	list<Part> partList;
public:
	Maintenance( const string desc = " " , double labourCharges = 0.0);
	void addPart(const Part& newPart);

	double getLabourCharges() const;
	void setLabourCharges(double labourCharges);
	const list<Part>& getPartList() const;
	virtual void input(void);
	virtual void display(void);
	virtual double price();
	friend ostream& operator<<(ostream &cout , Maintenance &other);
	friend istream& operator>>(istream &cin , Maintenance &other);

};
}


#endif /* MAINTENANCE_H_ */
