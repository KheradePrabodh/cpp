#include<iostream>
using namespace std;
class Point
{
private:
	int xPos;
	int yPos;
public:
	Point( int xPos = 0, int yPos = 0 )
	{
		this->xPos = xPos;
		this->yPos = yPos;
	}
	////////////////Pre-Increment////////////////
	Point operator++( void )
	{
		Point temp;
		temp.xPos = ++ this->xPos;
		temp.yPos = ++ this->yPos;
		return temp;
	}
	Point operator--( void )
	{
		Point temp;
		temp.xPos = -- this->xPos;
		temp.yPos = -- this->yPos;
		return temp;
	}
	////////////////Post-Increment////////////////
	Point operator++( int )	//Post-Increment
	{
		Point temp;
		temp.xPos = this->xPos ++;
		temp.yPos = this->yPos ++;
		return temp;
	}
	Point operator--( int )	//Post-Increment
	{
		Point temp;
		temp.xPos = this->xPos --;
		temp.yPos = this->yPos --;
		return temp;
	}
	void printRecord( void )
	{
		cout<<this->xPos<<"	"<<this->yPos<<endl;
	}
};

////////////////Post-Increment////////////////
int main( void )
{
	Point pt1(10,20);
	cout<<"pt1	:	";
	pt1.printRecord();
	cout<< endl;
	cout<<"Post_Decrement"<<endl;

	Point pt2 = pt1 ++;	//pt2 = pt1.operator++( )
	cout<<"pt1	:	";
	pt1.printRecord();	//11,21
	cout<<"pt2	:	";
	pt2.printRecord();	//10,20

	cout<<"Post_Decrement"<<endl;
	Point pt3 = pt1 --;	//pt2 = pt1.operator--( )
	cout<<"pt1	:	";
	pt1.printRecord();	//10,20
	cout<<"pt3	:	";
	pt3.printRecord();	//11,21
	return 0;
}


////////////////Pre-Increment////////////////
/*int main( void )
{
	Point pt1(10,20);
	cout<<"pt1	:	";
	pt1.printRecord();
	cout<< endl;
	cout<<"Pre_Icrement"<<endl;

	Point pt2 = ++ pt1; //pt2 = pt1.operator++( )
	cout<<"pt1	:	";
	pt1.printRecord();
	cout<<"pt2	:	";
	pt2.printRecord();

	cout<<"Pre_Decrement"<<endl;
	Point pt3 = -- pt1; //pt2 = pt1.operator--( )
	cout<<"pt1	:	";
	pt1.printRecord();
	cout<<"pt3	:	";
	pt3.printRecord();
	return 0;
}*/
