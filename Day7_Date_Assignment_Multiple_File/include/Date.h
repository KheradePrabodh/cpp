#ifndef DATE_H_
#define DATE_H_

#include<string>
using namespace std;

namespace date
{
	class Date
	{
	private:
		int day;
		int month;
		int year;
	public:
		void initDate();
		void acceptRecord();
		void printRecord();
		void addNumberOfDays( int );
		string dayOfWeek(void 	);
	};
}

#endif /* DATE_H_ */
