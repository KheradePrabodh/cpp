#include<iostream>
using namespace std;

#include "../include/Date.h"
using namespace date;


int menulist()
{
	int choice;
	cout << "0.Exit" << endl;
	cout << "1.Accept Record" << endl;
	cout << "2.Add Number Of Days" << endl;
	cout << "3.Print Record" << endl;
	cout << "Enter Your Choice	:	";
	cin >> choice;
	return choice;
}

int main(void)
{
	int choice,days;
	Date date;
	date.initDate();
	while((choice=menulist())!=0)
	{
		switch(choice)
		{
		case 1:
			date.acceptRecord();
			break;
		case 2:
			cout << "Enter Number Of Days To Be Added	:	";
			cin >>days;
			date.addNumberOfDays(days);
			break;
		case 3:
			date.printRecord();

		}
	}
}
