#include<iostream>
#include<string>

using namespace std;

namespace account
{
	class Account
	{
	public:
		string name;
		int number;
		string type;
		float balance;
	};
}

int count = 1000;

using namespace account;

namespace bank
{
	class Bank
	{
	private:
		int index;
		Account arr[ 5 ];
	public:
		void init();
		void accept_account_info( Account * );
		int create_account(   Account );
		float deposit( int , float );
		float withdraw( int , float );
		Account get_account_details( int );
		void print_account_info( Account * );
		void accept_account_number( int * );
		void accept_amount( float * );
		void print_account_number( int );
		void print_balance( float );
	};
}

using namespace bank;
	void Bank:: init()
	{
		index=-1;
	}
	void Bank:: accept_account_info( Account *ptraccount )
	{
		cout << endl;
		cout << "Name	:	";
		cin >> ptraccount->name ;
		cout << "Type	:	";
		cin >> ptraccount->type ;
		cout << "Balance	:	";
		cin>> ptraccount->balance ;
	}

	int Bank:: create_account(   Account account )
	{
		account.number = ++ count;
		arr[ ++index ] = account;
		return account.number;
	}

	float Bank:: deposit( int number, float amount )
	{
		for( int i = 0; i <= index; ++i )
		{
			if( arr[ i ].number == number  )
			{
				arr[ i ].balance += amount;
				return arr[ i ].balance;
			}
		}
		return 0;
	}

	float Bank:: withdraw( int number, float amount )
	{
		for( int i = 0; i <= index; ++ i )
		{
			if( arr[ i ].number == number  )
			{
				arr[ i ].balance -= amount;
				return arr[ i ].balance;
			}
		}
		return 0;
	}

	Account Bank:: get_account_details( int number )
	{
		Account account;
		for( int i = 0; i <= index; ++ i )
		{
			if( arr[ index ].number == number  )
			{
				account = arr[ index ];
			}
		}
		return account;
	}

	void Bank:: print_account_info( Account *ptraccount )
	{
		cout << endl;
		cout << "Account Holder Name	:	" << ptraccount->name << endl;
		cout << "Account Number		:	" << ptraccount->number << endl;
		cout << "Account Type		:	" << ptraccount->type << endl;
		cout << "Account Balance		:	" << ptraccount->balance << endl;
		cout << endl;
	}

	void Bank:: accept_account_number( int *number )
	{
		cout << "Account number	:	";
		cin >> *number ;
	}

	void Bank:: accept_amount( float *amount )
	{
		cout << "Amount	:	";
		cin >> *amount ;
	}

	void Bank:: print_account_number( int number )
	{
		cout << "Account number	:	" << number << endl;
	}

	void Bank:: print_balance( float balance )
	{
		cout << "Balance	:	"<< balance << endl;
	}

int menu_list( void )
{
	int choice;
	cout << endl;
	cout << "0.Exit" << endl;
	cout << "1.Create New Account" << endl;
	cout << "2.Deposit" << endl;
	cout << "3.Withdraw" << endl;
	cout << "4.Print Account details" << endl;
	cout << endl;
	cout << "Enter choice	:	";
	cin >> choice;
	return choice;
}

int main( void )
{
	int choice, accNumber;
	float balance, amount;
	Bank bank ;
	bank.init();
	Account account;
	while( ( choice = menu_list( ) ) )
	{
		switch( choice )
		{
		case 1:
			bank.accept_account_info(&account);
			accNumber = bank.create_account( account);
			bank.print_account_number(accNumber);
			break;
		case 2:
			bank.accept_account_number(&accNumber);
			bank.accept_amount( &amount );
			balance = bank.deposit( accNumber, amount );
			bank.print_balance(balance);
			break;
		case 3:
			bank.accept_account_number(&accNumber);
			bank.accept_amount(&amount );
			balance = bank.withdraw( accNumber, amount );
			bank.print_balance(balance);
			break;
		case 4:
			bank.accept_account_number(&accNumber);
			account = bank.get_account_details( accNumber);
			bank.print_account_info(&account);
			break;
		}
	}
	return 0;
}
