#include<iostream>
using namespace std;

/*void print( int number )
{
	cout<<"int : "<<number<<endl;
}
void print( float number )
{
	cout<<"float : "<<number<<endl;
}
int main( void )
{
	//print( 10 );	//	int : 10

	//print(10.5);	//	error: call of overloaded ‘print(double)’ is ambiguous

	//print(10.5f);	//  float	:	10.5

	print((int)10.5);	//	int : 10

	return 0;
}*/




void print( int number )
{
	cout<<"int : "<<number<<endl;
}
void print( int *number )
{
	cout<<"int* : "<<number<<endl;
}
int main( void )
{
	//print( 10 );	//	int : 10

	//int number = 10;
	//print( &number );	//	int* : 0x7ffeefb93438

	//print( NULL );	// #define NULL 0	//	error: call to 'print' is ambiguous
	//print( nullptr  ); //	int*:0x0

	//int *ptr=NULL; //	OK
	//int *ptr=nullptr; //	OK

	return 0;
}

