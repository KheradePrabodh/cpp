#include<iostream>
#include<cstring>
using namespace std;

class Array
{
private:
	int row;
	int column;
	int **arr;
public:
	Array( int row , int column );
	Array(const Array &other);
	void add(const Array &arr1 ,const Array &arr2 , char op);
	int getElement(const int row ,const int column )const;
	void setElement(int row , int column , int &element);
	int getColumn()const ;
	int getRow()const ;

	//void acceptRecord( void );
	//void printRecord( void );
	~Array( void );


};

Array:: Array( int row = 0, int column = 0 ): row(row),column(column),arr(NULL)
{
	if(this->row!=0 && this->column!=0)
	{
		this->arr = new int*[ row ];
		for( int i = 0 ; i < column ; i++ )
			this->arr[i]=new int[this->column];
	}
}
Array:: Array(const Array &other):row(other.row),column(other.column),arr(new int*[ this->row ])
{
	for( int i = 0 ; i < column ; i++ )
		this->arr[i]=new int[this->column];
	memcpy(this->arr,other.arr,(row*column*sizeof(int)));
}
Array:: ~Array( void )
{
	if( this->arr != NULL )
	{
		for( int i = 0 ; i < this->column ; i++ )
			delete[] arr[i];
		delete[] this->arr;
		this->arr = NULL;
	}
	//cout<<"~Array( void )"<<endl;
}


int Array:: getElement(const int row ,const int column) const{
	return this->arr[row][column];
}

void Array:: setElement(int row , int column , int &element){
	this->arr[row][column]=element;
}

int Array:: getColumn()const{
	return column;
}

int Array:: getRow()const{
	return row;
}
void Array:: add(const Array &arr1 ,const Array &arr2 , char op)
{
	for( int i = 0 ; i < this->column ; i++ )
		this->arr[i]=new int[this->column];
	if( op == '+' )
	{
		for( int i = 0 ; i < this->row ; i++ )
		{
			for(int j = 0 ; j < this->column ; j++ )
				this->arr[i][j] = arr1.getElement(i,j) + arr2.getElement(i,j);
		}
	}
	else if( op == '-' )
	{
		for( int i = 0 ; i < this->row ; i++ )
		{
			for(int j = 0 ; j < this->column ; j++ )
				this->arr[i][j] = arr1.getElement(i,j) - arr2.getElement(i,j);
		}
	}
	else if( op == '*' )
	{
		for( int i = 0 ; i < this->row ; i++ )
		{
			for(int j = 0 ; j < this->column ; j++ )
				this->arr[i][j] = arr1.getElement(i,j) * arr2.getElement(i,j);
		}
	}
	else if( op == '/' )
	{
		for( int i = 0 ; i < this->row ; i++ )
		{
			for(int j = 0 ; j < this->column ; j++ )
				this->arr[i][j] = arr1.getElement(i,j) / arr2.getElement(i,j);
		}
	}

}
void acceptRecord( Array &arr )
{
	for( int i = 0 ; i < arr.getRow() ; i++ )
	{
		for(int j = 0 ; j < arr.getColumn() ; j++ )
		{
			int element;
			cout<<"Enter Element	:	";
			cin>>element;
			arr.setElement(i,j,element);

		}
	}
}
void printRecord( const Array &arr )
{
	for( int i = 0 ; i < arr.getRow() ; i++ )
	{
		for(int j = 0 ; j < arr.getColumn() ; j++ )
		{
			cout<<arr.getElement(i,j)<<"\t";
		}
		cout<<endl;
	}
}


int main( void )
{
	int row,column;
	cout<<"Enter Number of Rows	:	";
	cin>>row;
	cout<<"Enter Number of Columns	:	";
	cin>>column;
	Array a1(row,column);
	acceptRecord(a1);
	printRecord(a1);
	cout<<endl<<endl;

	Array a2(a1);
	//a2.acceptRecord();
	printRecord(a2);
	cout<<endl<<endl;

	Array a3(row,column);
	a3.add(a1,a2,'+');
	cout<<"Addition	:	"<<endl;
	printRecord(a3);
	cout<<endl<<endl;

	a3.add(a1,a2,'-');
	cout<<"Subtraction	:	"<<endl;
	printRecord(a3);
	cout<<endl<<endl;

	a3.add(a1,a2,'*');
	cout<<"Multiplication	:	"<<endl;
	printRecord(a3);
	cout<<endl<<endl;

	a3.add(a1,a2,'/');
	cout<<"Division	:	"<<endl;
	printRecord(a3);
	cout<<endl<<endl;
	return 0;
}
