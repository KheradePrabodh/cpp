#include <stdio.h>


int main()
{
	int first=0;
	int second=1;
	int third=0;
	int count;
	printf("Enter number of terms of Fibonacci series to be displayed	:	");
	scanf("%d",&count);
	printf("First %d terms of Fibonacci Series	:	",count);
	printf("%d,%d,",first,second);
	for(int i=0;i<(count-2);i++)
	{
		third=first+second;
		printf("%d",third);
		if(i<(count-3))
		{
			printf(",");
		}
		first=second;
		second=third;
	}
}
