#include<stdio.h>

int num1 = 10;
namespace na
{
	int num2 = 20;
	namespace nb	//Nested Namespace
	{
		int num3 = 30;
	}
}


/*int main( void )
{

	printf("Num3	:	%d\n", nb::num3);//not ok
	return 0;
}*/


/*int main( void )
{

	printf("Num3	:	%d\n", na::nb::num3); //ok  30
	printf("Num3	:	%d\n", ::na::nb::num3); //ok  30
	return 0;
}*/

/*int main( void )
{

	printf("Num3	:	%d\n", na::num2); //ok //20
	printf("Num3	:	%d\n", ::na::num2); //ok //20
	return 0;
}*/


int main( void )
{
	printf("Num1	:	%d\n", num1);	//OK //10
	printf("Num1	:	%d\n", ::num1);	//OK //10
	return 0;
}
