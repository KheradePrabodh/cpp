#include<iostream>
using namespace std;

#include "../include/Stack.h"
using namespace stack;
int menulist()
{
	int choice;
	cout << "0.Exit" << endl;
	cout << "1.PUSH Element" << endl;
	cout << "2.POP Element" << endl;
	cout << "3.PEEK Element" << endl << endl;
	cout << "Enter your choice	:	";
	cin >> choice;
	return choice;
}

int main()
{
	Stack S;
	int choice;
	int element;
	while((choice=menulist())!=0)
	{
		switch(choice)
		{
		case 1:
			if(S.isFull()==false)
			{
				acceptElement(&element);
				S.pushElement(element);
			}
			else if(S.isFull()==true)
				cout << "Stack is full" << endl <<endl;
			break;
		case 2:
			if(S.isEmpty()==false)
			{
				int popped_element=S.popElement();
				cout << "Popped Element is	:	" << popped_element << endl << endl;
			}
			else if(S.isEmpty()==true)
				cout << "Stack is Empty" << endl << endl;
			break;
		case 3:
			if(S.isEmpty()==false)
			{
				int peeked_element=S.peekElement();
				cout << "Peeked Element		:	" << peeked_element << endl << endl;
			}
			else
			cout << "Stack is Empty" << endl << endl;
			break;

		}
	}
	return 0;
}
