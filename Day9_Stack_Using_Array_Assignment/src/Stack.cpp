#include<iostream>
using namespace std;

#include "../include/Stack.h"
using namespace stack;

Stack::Stack(void)
{
	this->top=-1;
}

bool Stack:: isFull(void)
{
	if (this->top==MAX-1)
		return true;
	else
		return false;
}

void Stack::pushElement(int element)
{
	++(this->top);
	this->arr[this->top]=element;
}

bool Stack:: isEmpty(void)
{
	if(this->top==-1)
		return true;
	else
		return false;
}

int Stack:: popElement(void)
{
	int popped_element=this->arr[this->top];
	--(this->top);
	return popped_element;
}

int Stack:: peekElement(void)
{
	int peeked_element=this->arr[top];
	return peeked_element;
}

void acceptElement(int*ptrElement)
{
	cout<< "Enter Element to push	:	";
	cin >> *ptrElement;
	cout << endl;
}
