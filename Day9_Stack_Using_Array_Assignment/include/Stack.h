#ifndef STACK_H_
#define STACK_H_

#define MAX 20
namespace stack
{
	class Stack
	{
	private:
		int arr[MAX];
		int top;

	public:
		Stack(void);
		bool isFull(void);
		void pushElement(int);
		bool isEmpty();
		int popElement();
		int peekElement();
	};
}

void acceptElement(int*);

#endif /* STACK_H_ */
