#include<iostream>
using namespace std;
struct Point
{
	int xPos;
	int yPos;
};

int main(void)
{
	struct Point pt1 = { 10, 20 };
	struct Point pt2 = { 30, 40 };
	struct Point pt3;

	//pt3 = pt1 + pt2;	//	not ok	//	error: no match for ‘operator+’ (operand types are ‘Point’ and ‘Point’)

	pt3.xPos = pt1.xPos + pt2.xPos;	//	OK
	pt3.yPos = pt1.yPos + pt2.yPos;	//	OK

	return 0;
}

/*int main( void )
{
	int num1 = 10;
	int num2 = 20;
	int result = num1 + num2;	//	ok
	cout<<"Result	:	"<<result<<endl;
	return 0;
}*/
