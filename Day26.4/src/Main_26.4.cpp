#include<iostream>
using namespace std;
class Shape
{
protected:
	float area;
public:
	Shape( void )
	{
		this->area = 0;
	}

	virtual void acceptRecord( void ) = 0;

	virtual float calculateArea( void ) = 0;

	void printRecord( void )const
	{
		cout<<"Area	:	"<<this->area<<endl;
	}
};
class Rectangle : public Shape
{
private:
	float length;
	float breadth;
public:
	Rectangle( void )
	{
		this->length = 0;
		this->breadth = 0;
	}
	void acceptRecord( void )
	{
		cout<<"Length	:	";
		cin>>this->length;
		cout<<"Breadth	:	";
		cin>>this->breadth;
	}
	float calculateArea( void )
	{
		this->area = this->length * this->breadth;
		return this->area;
	}
};
class Math
{
public:
	static const float PI;
public:
	static float pow( float base, int index )
	{
		float result = 1;
		for( int count = 1; count <= index; ++ count )
			result = result * base;
		return result;
	}
};
const float Math::PI=3.142;
class Circle : public Shape
{
private:
	float radius;
public:
	Circle( void )
	{
		this->radius = 0;
	}
	void acceptRecord( void )
	{
		cout<<"Radius	:	";
		cin>>this->radius;
	}
	float calculateArea( void )
	{
		this->area = Math::PI * Math::pow(this->radius,2);
		return this->area;
	}
};
class ShapeFactory
{
public:
	static Shape* getInstance( int choice )
	{
		Shape *ptr = NULL;
		switch( choice )
		{
		case 1:
			ptr = new Rectangle( );	//Upcasting
			break;
		case 2:
			ptr = new Circle( );	//Upcasting
			break;
		}
		return ptr;
	}
	static int menuList( void )
	{
		int choice;
		cout<<"0.Exit"<<endl;
		cout<<"1.Rectangle"<<endl;
		cout<<"2.Circle"<<endl;
		cout<<"Enter choice	:	";
		cin>>choice;
		return choice;
	}
};
int main( void )
{
	int choice;
	while( ( choice = ShapeFactory::menuList() ) != 0 )
	{
		Shape *ptr =  ShapeFactory::getInstance(choice);
		if( ptr != NULL )
		{
			ptr->acceptRecord( );	//Runtime Polymorphism
			ptr->calculateArea( );	//Runtime Polymorphism
			ptr->printRecord( );
			delete ptr;
		}
	}
	return 0;
}
