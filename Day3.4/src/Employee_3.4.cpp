#include "../include/Employee_3.4.h"

#include<stdio.h>

void Employee::acceptRecord()
	{
		printf("Name	:	");
		scanf("%[^\n]%*c", name);
		printf("Empid	:	");
		scanf("%d", &empid);
		printf("Salary	:	");
		scanf("%f", &salary);
	}

void Employee::printRecord()
{
	printf("Name	:	%s\n", name);
	printf("Empid	:	%d\n", empid);
	printf("Salary	:	%.2f\n", salary);
}
