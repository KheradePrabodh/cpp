#include<iostream>
using namespace std;
class Point
{
private:
	int xPos;
	int yPos;
public:
	Point( int xPos = 0, int yPos = 0 )
	{
		this->xPos = xPos;
		this->yPos = yPos;
	}
	bool operator==( Point &other )
	{
		return this->xPos == other.xPos && this->yPos == other.yPos;
	}
	bool operator!=( Point &other )
	{
		return this->xPos != other.xPos && this->yPos != other.yPos;
	}
	bool operator<( Point &other )
	{
		return this->xPos < other.xPos && this->yPos < other.yPos;
	}
	bool operator<=( Point &other )
	{
		return this->xPos <= other.xPos && this->yPos <= other.yPos;
	}
	bool operator>( Point &other )
	{
		return this->xPos > other.xPos && this->yPos > other.yPos;
	}
	bool operator>=( Point &other )
	{
		return this->xPos >= other.xPos && this->yPos >= other.yPos;
	}
	void printRecord( void )
	{
		cout<<this->xPos<<"	"<<this->yPos<<endl;
	}
};
int main( void )
{
	Point pt1(10,20);
	Point pt2(10,20);
	pt1.printRecord();
	pt2.printRecord();
	cout<<( pt1 == pt2  ? "Equal" : "Not Equal" )<<endl;
	Point pt3(10,20);
	Point pt4(30,40);
	pt3.printRecord();
	pt4.printRecord();
	cout<<( pt3 != pt4  ? "Not Equal" : "Equal" )<<endl;
	Point pt5(10,20);
	Point pt6(30,40);
	pt5.printRecord();
	pt6.printRecord();
	cout<<( pt5 < pt6  ? "Less" : "Greater" )<<endl;
	Point pt7(10,20);
	Point pt8(30,40);
	pt7.printRecord();
	pt8.printRecord();
	cout<<( pt7 <= pt8  ? "Less Or Equal" : "Greater" )<<endl;
	Point pt9(30,40);
	Point pt10(10,20);
	pt9.printRecord();
	pt10.printRecord();
	cout<<( pt9 > pt10  ? "Greater" : "Less" )<<endl;
	Point pt11(30,40);
	Point pt12(10,20);
	pt11.printRecord();
	pt12.printRecord();
	cout<<( pt11 >= pt12  ? "Greater Or Equal" : "less" )<<endl;
	return 0;
}
int main3( void )
{
	Point pt1(10,20);
	Point pt2(10,20);
	if( pt1 == pt2 )
		cout<<"Equal"<<endl;
	else
		cout<<"Not Equal"<<endl;
	return 0;
}
int main2( void )
{
	Point pt1(10,20);
	Point pt2(10,20);
	bool status = pt1 == pt2; //status = pt1.operator==( pt2 );
	if( status  )
		cout<<"Equal"<<endl;
	else
		cout<<"Not Equal"<<endl;
	return 0;
}
int main1( void )
{
	Point pt1(10,20);
	Point pt2(10,20);
	bool status = pt1 == pt2; //status = pt1.operator==( pt2 );
	if( status == true )
		cout<<"Equal"<<endl;
	else
		cout<<"Not Equal"<<endl;
	return 0;
}
