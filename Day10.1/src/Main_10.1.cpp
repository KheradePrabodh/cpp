#include<cstdlib>
#include<iostream>
using namespace std;

/*class Test
{
private:
	int num1;
	int num2;
	int num3;
public:
	void printRecord( void )
	{
		cout << "Num1	:	" << this->num1 << endl;
		cout << "Num2	:	" << this->num2 << endl;
		cout << "Num3	:	" << this->num3 << endl;
	}
};

int main( void )
{
	Test t;
	t.printRecord();	//	garbage value
	return 0;
}*/



/*class Test
{
private:
	int num1;
	int num2;
	int num3;
public:
	Test()
	{
		cout << "inside ctor" << endl;
	}

	void printRecord( void )
	{
		cout << "Num1	:	" << this-> num1 << endl;
		cout << "Num2	:	" << this-> num2 << endl;
		cout << "Num3	:	" << this-> num3 << endl;
	}
};

Test t;
int main( void )
{
	cout << "inside main" << endl;
	t.printRecord(); // 0
	return 0;
}*/



/*class Test
{
private:
	int num1;
	int num2;
	int num3;
public:
	Test()
	{

	}

	void printRecord( void )
	{
		cout << "Num1	:	" << this->num1 << endl;
		cout << "Num2	:	" << this->num2 << endl;
		cout << "Num3	:	" << this->num3 << endl;
	}
};

int main( void )
{
	static Test t;
	t.printRecord(); // 0
	return 0;
}*/



/*class Test
{
private:
	int num1;
	int num2;
	int num3;
public:
	Test( void )
	{
	cout << "Inside ctor" << endl;
		this->num2 = num1;
		this->num1 = 10;
		this->num3 = num2;
	}
	void printRecord( void )
	{
		cout << "Num1	:	" << this ->num1 << endl;
		cout << "Num2	:	" << this ->num2 << endl;
		cout << "Num3	:	" << this ->num3 << endl;
	}
};
Test t1;	//	data seg
int main( void )
{
	static Test t2;	//	data seg
	Test t3;	//	stack seg
	t.printRecord();
	return 0;
}*/



/*class Test
{
private:
	int num1;
	int num2;
	int num3;
public:
	Test( void )
	{
	cout << "Inside ctor" << endl;
		this->num2 = num1;
		this->num1 = 10;
		this->num3 = num2;
	}
	void printRecord( void )
	{
		cout << "Num1	:	" << this ->num1 << endl;
		cout << "Num2	:	" << this ->num2 << endl;
		cout << "Num3	:	" << this ->num3 << endl;
	}
	~Test(void)
	{
		cout << "Inside dtor" << endl;
	}
};

int main(void)
{
	Test *ptr=(Test*) malloc (sizeof(Test));
	ptr->printRecord();
	free(ptr);
}*/




/*class Test
{
private:
	int num1;
	int num2;
	int num3;
public:
	Test( void )
	{

	this->num3 = num1;
	this->num1 = 10;
	this->num2 = num3;
	}

	Test( void )
	{

	this->num2 = num1;
	this->num1 = 10;
	this->num3 = num2;
	}

	void printRecord( void )
	{
		cout << "Num1	:	" << this ->num1 << endl;	//	Num1	:	10
		cout << "Num2	:	" << this ->num2 << endl;	//	Num2	:garbage value
		cout << "Num3	:	" << this ->num3 << endl;	//	Num3	:garbage value
	}
};

int main(void)
{
	Test t;
	t.printRecord();
	return 0;
}*/



class Test
{
private:
	int num1;
	int num2;
	int num3;
public:
	/*Test( void )
	{
	this->num2 = num1;
	this->num1 = 10;
	this->num3 = num2;
	}*/
	Test( void ) : num2( num1 ), num1( 10 ), num3( num2 )	//	Constructor's member initializer list.
	{	}
	void printRecord( void )
	{
		cout << "Num1	:	" << this ->num1 << endl;	//	Num1	:	10
		cout << "Num2	:	" << this ->num2 << endl;	//	Num1	:	10
		cout << "Num3	:	" << this ->num3 << endl;	//	Num1	:	10
	}
};

int main(void)
{
	Test t;
	t.printRecord();
	return 0;
}
