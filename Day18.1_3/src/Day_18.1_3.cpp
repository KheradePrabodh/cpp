#include<iostream>
using namespace std;
class Point
{
private:
	int xPos;
	int yPos;
public:
	Point( int xPos = 0, int yPos = 0 ) : xPos( xPos ), yPos( yPos )
	{	}
	void printRecord( void )
	{
		cout<<this->xPos<<"	"<<this->yPos<<endl;
	}
};
int main( void )
{
	Point pt1(10,20);
	Point pt2(30,40);
	Point pt3;
	pt3 = pt1 + pt2;	//	not ok	//	error: no match for ‘operator+’ (operand types are ‘Point’ and ‘Point’)
	pt3.printRecord();
	return 0;
}
