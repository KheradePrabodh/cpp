#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	//Complex *const this = &c1
	Complex( void )	//	complex c1;
	{
		cout<<"Complex( void )"<<endl;
		this->real = 0;
		this->imag = 0;
	}

	//Complex *const this = &c1
	Complex( int value )	//	complex c2(10);
	{
		cout<<"Complex( int value )"<<endl;
		this->real = value;
		this->imag = value;
	}

	//Complex *const this = &c1
	Complex( int real, int imag )	//	complex c3(10,20);
	{
		cout<<"Complex( int real, int imag )"<<endl;
		this->real = real;
		this->imag = imag;
	}

	//Complex *const this = &c1
	/*void initComplex( int real, int imag )
	{
		this->real = real;
		this->imag = imag;
	}*/

	//Complex *const this = &c1
	void acceptRecord( void )
	{
		cout<<"Real Number	:	";
		cin>>this->real;
		cout<<"Imag Number	:	";
		cin>>this->imag;
	}

	//Complex *const this = &c1
	void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};

int main( void )
{
	Complex c1;
	/*c1.initComplex( 0, 	0 );
	c1.acceptRecord( );	//50,60
	c1.initComplex( 0, 	0 );
	c1.printRecord( );*/

	Complex c2(10);
	Complex c3(10,20);
	c1.printRecord();
	c2.printRecord();
	c3.printRecord();
	return 0;
}

/*int main( void )
{
	Complex c1;	//Ok : Complex::Complex( )
	//c1.Complex( );	//Not OK

	Complex *ptr = &c1; //OK
	//ptr->Complex();	//Not Ok

	Complex &c2 = c1;	//OK
	//c2.Complex( );	//Not OK
	return 0;
}*/


/*int main( void )
{
	Complex c1;
	Complex c2(10);
	Complex c3(10,20);
	c1.printRecord();
	c2.printRecord();
	c3.printRecord();
	return 0;
}*/
