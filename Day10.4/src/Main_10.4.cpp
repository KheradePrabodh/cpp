#include<cstdlib>
#include<iostream>
using namespace std;
class Test
{
private:
	int num1;
	int num2;
	int num3;
public:
	/*Test( void ) : num1( 10 ), num2( 20 ), num3( 30 ) 	// not recommended(should not be used)
	{
		this->num1 = 50;	// num1	:	10 is overwritten by 50
		this->num2 = 60;	// num2	:	20 is overwritten by 60
		this->num3 = 70;	// num3	:	30 is overwritten by 70
	}*/
	Test( void ) : num1( 10 ), num3( 30 ) 	// ok(can be used as array cannot be initialise using initializer list)
	{
		this->num2 = 20;
	}
	void printRecord( void )
	{
		cout<<"Num1	:	"<<this->num1<<endl;	//	Num1	:	50
		cout<<"Num2	:	"<<this->num2<<endl;	//	Num2	:	60
		cout<<"Num3	:	"<<this->num3<<endl;	//	Num3	:	70
	}
};
int main( void )
{
	Test t1;
	t1.printRecord();
	return 0;
}
