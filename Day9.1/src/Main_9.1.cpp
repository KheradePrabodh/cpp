#include<iostream>
using namespace std;

/*class Complex
{
private:
	int real;
	int imag;
public:
	Complex(  )
	{
		this->real = 0;
		this->imag = 0;
	}
	Complex( int real, int imag )
	{
		this->real = real;
		this->imag = imag;
	}
	void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};


int main( void )
{
	Complex c1;
	c1.printRecord();
	return 0;
}
*/

/*class Complex
{
private:
	//int *ptr; suppose compiler added
	int real;
	int imag;
public:
	Complex() // compiler defined default ctor
	{
		ptr=NULL;
	}
	Complex(  )	//	Complex c2;
	{
		this->real = 0;
		this->imag = 0;
	}
	Complex( int real, int imag )	//	Complex c1( 10, 20 );
	{
		this->real = real;
		this->imag = imag;
	}
	void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};
int main( void )
{
	Complex c1;
	c1.printRecord();
	return 0;
}*/

/*int main( void )
{
	Complex c1( 10, 20 );
	c1.printRecord();
	Complex c2;
	c2.printRecord();
	return 0;
}*/


/*class Complex
{
private:
	//v-ptr
	int real;
	int imag;
public:
	virtual void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};
int main( void )
{
	Complex c1;
	cout<<sizeof( c1 )<<endl;
	return 0;
}*/




/*class Complex  // Aggregate class
{
public:
	int real;
	int imag;
public:
	void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};
int main( void )
{
	Complex c1;
	c1.printRecord();
	return 0;
}*/
