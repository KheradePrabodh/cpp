#include<iostream>
using namespace std;


void swap( int &x, int &y )//(compiler)void swap( int *const x, int *const y )
{
	int temp = x;	//(compiler)int temp = *x;
	x = y;			//(compiler)*x = *y;
	y = temp;		//(compiler)*y = temp;
}
int main( void )
{
	int a = 10;
	int b = 20;
	swap( a, b );	//int &x = a	//int &y = b
	cout<<"a	:	"<<a<<endl;
	cout<<"b	:	"<<b<<endl;
	return 0;
}

/*int main( void )
{
	int array_a1[ 3 ] = { 10, 20, 30 };
	int (&arrary_a2)[ 3 ] = array_a1;

	for( int index = 0; index < 3; ++ index )
		cout<<arrary_a2[ index ]<<endl;		//Output	=10
											//			 20
											//			 30				//we can give reference to array
	return 0;
}*/

/*int main( void )
{
	int number = 10;
	int *pointer1 = &number;
	int *&ref_pointer1 = pointer1;

	cout<<&pointer1<<endl;		//Output	:	0x7ffe386449b8
	cout<<&ref_pointer1<<endl;	//Output	:	0x7ffe386449b8
	return 0;
}*/

/*class Test
{
private:
	char &ch1;
public:
	Test( char &ch2 ) : ch1( ch2  )
	{	}
};
int main( void )
{
	char ch = 'A';
	Test t(ch);
	size_t size =  sizeof( t );
	cout<<"Size	:	"<<size<<endl;	// 8 Bytes //proof reference gets memory
									//reference is internally constant pointer
	return 0;
}*/

/*int main( void )
{
	int a = 200;
	int a1 = 500;
	int &a2 = a;
	a2 = a1;	// value is copied ,we cannot change referent once reference is initialized
	++ a2;

	cout<<"a	:	"<<a<<endl;		//Output	=	a	:	501
	cout<<"a1	:	"<<a1<<endl;	//Output	=	a1	:	500
	cout<<"a2	:	"<<a2<<endl;	//Output	=	a2	:	501
	return 0;
}*/

/*int main( void )
{
	int a = 200;
	int &a1 = a;
	int &a2 = a1; //or &a2=a;  // both are same


	cout<<"a	:	"<<a<<endl;		//Output	=	a	:	200
	cout<<"a1	:	"<<a1<<endl;	//Output	=	a1	:	200
	cout<<"a2	:	"<<a2<<endl;	//Output	=	a2	:	200
	return 0;
}*/

/*int main( void )
{
	int a = 200;
	int &a1 = a;
	const int &a2 = a;

	++ a;
	++ a1;
	//++ a2;	//Not OK  //error: increment of read-only reference ‘a2’

	cout<<"a	:	"<<a<<endl;		//output	=	a	:	12
	cout<<"a1	:	"<<a1<<endl;	//output	=	a1	:	12
	cout<<"a2	:	"<<a2<<endl;	//output	=	a2	:	12
	return 0;
}*/

/*int main( void )
{
	int a = 200;
	int &a1 = a;

	++ a;
	++ a1;

	cout<<"a	:	"<<a<<endl;		//output	=a	:	202
	cout<<"a1	:	"<<a1<<endl;	//output	=a1	:	202
	return 0;
}*/

/*int main( void )
{
	int a = 200;		//referent variable
	int &a1 = a;	//reference variable
	//int &a2 = 100;	//Not OK  //error: cannot bind non-const lvalue reference of type ‘int&’ to an rvalue of type ‘int’

	return 0;
}*/

/*int main( void )
{
	int a = 200;		//referent variable
	int &a1 = a;	//reference variable

	cout<<"a	:	"<< a <<"	"<<&a<<endl; //output	=a	:	10	0x7ffc3b4fe1fc
	cout<<"a1	:	"<< a1 <<"	"<<&a1<<endl; //output	=a1	:	10	0x7ffc3b4fe1fc
	return 0;
}*/
