#include<iostream>
using namespace std;

void displayNum( int Num )
{
	cout<<"Num	:	"<<Num<<endl;
}
void displayNum( int &Num )
{
	cout<<"&Num	:	"<<Num<<endl;
}


int main( void )
{
	//displayNum( 200); // ok output:200	// as displayNum( int Num ) will be called
						// because reference is not used to refer constant in this case 200
	//int Num = 10;
	//displayNum( Num ); //  error: call of overloaded ‘displayNum(int&)’ is ambiguous

	return 0;
}
