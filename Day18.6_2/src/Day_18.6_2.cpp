#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( int real = 0, int imag = 0 )
	{
		this->real = real;
		this->imag = imag;
	}
	void printRecord( void )
	{
		cout<<this->real<<"	"<<this->imag<<endl;
	}
	friend Complex operator+( Complex &c1, int value )	//	c2 = operator+( c1, 5 );
	{
		Complex temp;
		temp.real = c1.real + value;
		temp.imag = c1.imag + value;
		return temp;
	}
	friend Complex operator+( int value, Complex &c1 )	//	c2 = operator+( 5, c1 );
	{
		Complex temp;
		temp.real = value + c1.real;
		temp.imag = value + c1.imag;
		return temp;
	}
};

/*int main( void )
{
	Complex c1( 10, 20 );
	cout << "c1	:	";
	c1.printRecord();
	//Complex c2 = 5 + c1;	//c2 = 5.operator+( c1 ); //Not OK
	Complex c2 = 5 + c1;	//c2 = operator+( 5, c1 );	//ok
	cout << "c2	:	";
	c2.printRecord();
	return 0;
}*/

int main( void )
{
	Complex c1( 10, 20 );
	cout << "c1	:	";
	c1.printRecord();

	//Complex c2 = c1 + 5;	//c2 = c1.operator+( 5 );
	Complex c2 = c1 + 5;	//c2 = operator+( c1, 5 );

	cout << "c2	:	";
	c2.printRecord();
	return 0;
}
