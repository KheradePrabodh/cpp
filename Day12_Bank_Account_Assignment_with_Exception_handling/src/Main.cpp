#include<iostream>
#include<string>

using namespace std;

namespace account
{
	class Account
	{
	private:
		string name;
		int number;
		string type;
		float balance;
	public:
		Account(void)
		{
			cout << "inside ctor" << endl;
			this->name="";
			this->number=0;
			this->type="";
			this->balance=0;
		};


		string& getName()
		{
			return this->name;
		}
		void setName(string &name)
		{
			this->name = name;
		}


		int getNumber()
		{
			return this->number;
		}
		void setNumber(int number)
		{
			this->number = number;
		}


		float getBalance()
		{
			return this->balance;
		}
		void setBalance(float balance)
		{
			this->balance = balance;
		}


		string& getType()
		{
			return type;
		}
		void setType(string &type)
		{
			this->type = type;
		}
	};
}

int count = 1000;

using namespace account;
namespace bank
{
	class InvalidAccountNumberException
	{
	private:
		string message;
	public:
		InvalidAccountNumberException(string message)throw():message(message)
		{	}
		const string& getMessage(void)const throw()
			{
				return message;
			}
	};
	class InsuffiecientBalanceException
	{
	private:
		string message;
	public:
		InsuffiecientBalanceException(string message)throw():message(message)
		{	}
		const string& getMessage(void)const throw()
		{
			return message;
		}
	};
	class IllegalArgumentException
	{
	private:
		string message;
	public:
		IllegalArgumentException(string message)throw():message(message)
		{	}
		const string& getMessage(void)const throw()
		{
			return message;
		}
	};
	class Bank
	{
	private:
		int index;
		Account arr[ 5 ];
	public:
		Bank(void);
		void accept_account_info( Account * );
		int create_account(  Account );
		float deposit( int , float )throw(IllegalArgumentException);
		float withdraw( int number, float )throw(InsuffiecientBalanceException);
		void get_account_details( int , Account& )throw();
		void print_account_info( Account * );
		void accept_account_number( int * );
		void accept_amount( float * );
		void print_account_number( int );
		void print_balance( float );
		bool checkAccountNumber(int &accNumber)throw(InvalidAccountNumberException)
		{
			int flag=0;
			for(int index=0;index<=this->index;index++)
			{
				if( this->arr[index].getNumber() == accNumber  )
				{
					flag=1;
					break;
				}
			}
			if (flag==0)
				throw InvalidAccountNumberException("Account Number Is Invalid");
			return true;
		}
	};
}

using namespace bank;
Bank :: Bank(void)
{
	this->index=-1;
};

void Bank :: accept_account_info( Account *ptraccount )
{
	string name,type;
	float balance;
	cout << endl;
	cout << "Name	:	";
	cin >> name ;
	ptraccount->setName(name);
	cout << "Type	:	";
	cin >>type;
	ptraccount->setType(type);
	cout << "Balance	:	";
	cin>>balance;
	if (balance<0)
		throw IllegalArgumentException("Amount must be greater than 0");
	ptraccount->setBalance(balance);
}

int Bank :: create_account(  Account account )
{
	account.setNumber(++ count);
	this->arr[++this->index] = account;
	return account.getNumber();
}

float Bank :: deposit( int number, float amount )throw(IllegalArgumentException)
{
	for( int index = 0; index <= this->index; ++ index )
	{
		if( this->arr[index].getNumber() == number  )
		{
			if(amount<0)
				throw IllegalArgumentException("Amount must be greater than 0");
			this->arr[index].setBalance(this->arr[index].getBalance()+amount);
			return this->arr[index].getBalance();
		}
	}
	return 0;
}

float Bank :: withdraw( int number, float amount )throw(InsuffiecientBalanceException)
{
	for( int index = 0; index <= this->index; ++ index )
	{
		if( this->arr[index].getNumber() == number  )
		{
			if(amount>arr[index].getBalance())
				throw InsuffiecientBalanceException("Insufficient Balance");
			this->arr[index].setBalance(this->arr[index].getBalance()-amount);
			return this->arr[index].getBalance();
		}
	}
	return 0;
}

void Bank :: get_account_details( int number,Account &account )throw()
{
	for( int index = 0; index <= this->index; ++ index )
	{
		if( this->arr[index].getNumber() == number  )
		account = this->arr[index];
	}
}

void Bank :: print_account_info( Account *ptraccount )
{
	cout << endl;
	cout << "Account Holder Name	:	" << ptraccount->getName() << endl;
	cout << "Account Number		:	" << ptraccount->getNumber() << endl;
	cout << "Account Type		:	" << ptraccount->getType()<< endl;
	cout << "Account Balance		:	" << ptraccount->getBalance() << endl;
	cout << endl;
}

void Bank :: accept_account_number( int *number )
{
	cout << "Account number	:	";
	cin >> *number ;
}

void Bank :: accept_amount( float *amount )
{
	cout << "Amount	:	";
	cin >> *amount ;
}

void Bank :: print_account_number( int number )
{
	cout << "Account number	:	" << number << endl;
}

void Bank :: print_balance( float balance )
{
	cout << "Balance	:	"<< balance << endl;
}
int menu_list( void )
{
	int choice;
	cout << endl;
	cout << "0.Exit" << endl;
	cout << "1.Create New Account" << endl;
	cout << "2.Deposit" << endl;
	cout << "3.Withdraw" << endl;
	cout << "4.Print Account details" << endl;
	cout << endl;
	cout << "Enter choice	:	";
	cin >> choice;
	return choice;
}

int main( void )
{
	int choice, accNumber;
	float balance, amount;
	Bank bank;
	Account account;
	while( ( choice = menu_list( ) ) )
	{
		try
		{
			switch( choice )
			{
			case 1:
				bank.accept_account_info(&account);
				accNumber = bank.create_account( account);
				bank.print_account_number(accNumber);
				break;
			case 2:
				bank.accept_account_number(&accNumber);
				if(bank.checkAccountNumber(accNumber))
				{
					bank.accept_amount( &amount );
					balance = bank.deposit( accNumber, amount );
					bank.print_balance(balance);
				}
				break;
			case 3:
				bank.accept_account_number(&accNumber);
				if(bank.checkAccountNumber(accNumber))
				{
					bank.accept_amount(&amount );
					balance = bank.withdraw( accNumber, amount );
					bank.print_balance(balance);
				}
				break;
			case 4:
				bank.accept_account_number(&accNumber);
				if(bank.checkAccountNumber(accNumber))
				{
					bank.get_account_details( accNumber,account);
					bank.print_account_info(&account);
				}
				break;
			}
		}
		catch(InvalidAccountNumberException &ex)
		{
			cout << ex.getMessage() << endl;
		}
		catch(IllegalArgumentException &ex)
		{
			cout << ex.getMessage() << endl;
		}
		catch(InsuffiecientBalanceException &ex)
		{
			cout << ex.getMessage() << endl;
		}
		catch( ... )
		{
			cout << "Exception" << endl;
		}
	}
	return 0;
}
