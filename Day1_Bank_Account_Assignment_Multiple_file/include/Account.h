#ifndef ACCOUNT_H_
#define ACCOUNT_H_

struct Account
{
	char name[ 50 ];
	int number ;
	char type[50];
	float balance;
};

int accept_account_info( struct Account * );
void print_account_info( struct Account *);
float deposit(struct Account *,float );
float withdraw(struct Account *,float );

#endif /* ACCOUNT_H_ */
