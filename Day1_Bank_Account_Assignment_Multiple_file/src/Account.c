#include<stdio.h>
#include<string.h>
#include "../include/Account.h"


extern int k;
extern int acc_number;

int accept_account_info( struct Account *ptr )
{
	int acc_type;
	char s1[]="Savings";
	char s2[]="Current";
	printf("Account Holder Name	:	");
	scanf("%s",ptr[k].name);
	ptr[k].number=acc_number++;
	printf("Account Type\n1.Savings\n2.Current	:	");
	scanf("%d",&acc_type);
	if(acc_type==1)
	{
		strcpy(ptr[k].type,s1);
	}
	else if(acc_type==2)
	{
		strcpy(ptr[k].type,s2);
	}
	printf("Starting Amount to be deposited	:	");
	scanf("%f", &ptr[k].balance);
	return ptr[k].number;
}

void print_account_info( struct Account *ptr)
{
		printf("Account Holder Name	:	%s\n", ptr->name );
		printf("Account Number	:	%d\n", ptr->number );
		printf("Account Type	:	%s\n", ptr->type );
		printf("Account Balance	:	%f\n", ptr->balance );
}

float deposit(struct Account *ptr,float amount)
{
	ptr->balance=ptr->balance+amount;
	return ptr->balance;
}

float withdraw(struct Account *ptr,float amount)
{
		if(amount<=ptr->balance)
		{
			ptr->balance=ptr->balance-amount;
		}
		else
		{
			printf("No Enough Balance\n");
		}

	return ptr->balance;
}

