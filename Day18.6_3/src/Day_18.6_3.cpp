#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( int real = 0, int imag = 0 )
	{
		this->real = real;
		this->imag = imag;
	}
	void printRecord( void )
	{
		cout<<this->real<<"	"<<this->imag<<endl;
	}

	 friend bool operator==( Complex &c1, Complex &c2 )
	{
		return c1.real == c2.real && c1.imag == c2.imag;
	}
	 friend bool operator!=( Complex &c3, Complex &c4 )
	{
		return c3.real != c4.real && c3.imag != c4.imag;
	}
	 friend bool operator<( Complex &c5, Complex &c6 )
	{
		return c5.real < c6.real && c5.imag < c6.imag;
	}
	 friend bool operator<=( Complex &c7, Complex &c8 )
	{
		return c7.real <= c8.real && c7.imag <= c8.imag;
	}
	 friend bool operator>( Complex &c9, Complex &c10 )
	{
		return c9.real > c10.real && c9.imag > c10.imag;
	}
	 friend bool operator>=( Complex &c11, Complex &c12 )
	{
		return c11.real >= c12.real && c11.imag >= c12.imag;
	}

};

int main( void )
{
	Complex c1( 10, 20 );
	Complex c2( 10, 20 );
	cout << "c1	:	";
	c1.printRecord();
	cout << "c2	:	";
	c2.printRecord();
	cout << ((c1==c2)?"Equal":"Not Equal") << endl;
	cout << endl;

	Complex c3( 10, 20 );
	Complex c4( 30, 40 );
	cout << "c3	:	";
	c3.printRecord();
	cout << "c4	:	";
	c4.printRecord();
	cout << ((c3!=c4)?"Not Equal":"Equal") << endl;
	cout << endl;

	Complex c5( 10, 20 );
	Complex c6( 30, 40 );
	cout << "c5	:	";
	c5.printRecord();
	cout << "c6	:	";
	c6.printRecord();
	cout << ((c5<c6)?"Less":"Greater") << endl;
	cout << endl;

	Complex c7( 10, 20 );
	Complex c8( 10, 20 );
	cout << "c7	:	";
	c7.printRecord();
	cout << "c8	:	";
	c8.printRecord();
	cout << ((c7<=c8)?"Less Or Equal":"Greater") << endl;
	cout << endl;

	Complex c9( 30, 40 );
	Complex c10( 10, 20 );
	cout << "c9	:	";
	c9.printRecord();
	cout << "c10	:	";
	c10.printRecord();
	cout << ((c9>c10)?"Greater":"Less") << endl;
	cout << endl;

	Complex c11( 40, 20 );
	Complex c12( 30, 10 );
	cout << "c11	:	";
	c11.printRecord();
	cout << "c12	:	";
	c12.printRecord();
	cout << ((c11>=c12)?"Greater Or Equal":"Less") << endl;
	cout << endl;

	return 0;
}
