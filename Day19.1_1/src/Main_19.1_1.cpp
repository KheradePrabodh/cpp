#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( void )
	{
		this->real = 0;
		this->imag = 0;
	}
	Complex( int real, int imag )
	{
		this->real = real;
		this->imag = imag;
	}
	/*void operator<<(ostream &cout)	//	 Not Recommended
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}*/
	friend ostream& operator<<( ostream &cout,const Complex &other )
	{
		cout<<"Real Number	:	"<<other.real<<endl;
		cout<<"Imag Number	:	"<<other.imag<<endl;
		return cout;
	}
};

/*int main( void )
{
	Complex c1(10,20);
	c1<<cout;	//c1.operator<<(cout);	//	Not recommended
	return 0;
}*/

/*int main( void )
{
	Complex c1(10,20);
	Complex c2(30,40);
	cout<<c1<<c2;	//operator<<(operator<<( cout, c1),c2);
	return 0;
}*/

/*int main( void )
{
	Complex c1(10,20);
	cout<<c1; //operator<<( cout, c1 )
	return 0;
}*/

/*int main()
{
	//ostream out;	//	error: ‘std::basic_ostream<_CharT, _Traits>::basic_ostream()
					//[with _CharT = char; _Traits = std::char_traits<char>]’ is protected within this context
	//ostream out=cout;	//	error: use of deleted function ‘std::basic_ostream<_CharT, _Traits>::basic_ostream(const std::basic_ostream<_CharT, _Traits>&)
						//[with _CharT = char; _Traits = std::char_traits<char>]’

	ostream &out=cout;	//	ok
	return 0;
}*/
