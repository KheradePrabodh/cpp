#include<iostream>
using namespace std;

class Outer	//Top Level class
{
private:
	int num1;
public:
	class Inner	//Nested class
	{
	public:
		Inner( void )
		{
			cout<<"Inner( void )"<<endl;
		}
	};
public:
	Outer( void )
	{
		this->num1 = 10;
	}
	void print( void )
	{
		cout<<"Num1	:	"<<this->num1<<endl;
	}
};
int main( void )
{
	Outer out;
	out.print();
	return 0;
}
