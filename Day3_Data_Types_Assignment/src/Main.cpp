#include<stdio.h>

int main(void)
{
	printf("Size Of void	:	%lu\n",sizeof(void));
	printf("Size Of bool	:	%lu\n",sizeof(bool));
	printf("Size Of char	:	%lu\n",sizeof(char));
	printf("Size Of wchar_t	:	%lu\n",sizeof(wchar_t));
	printf("Size Of int	:	%lu\n",sizeof(int));
	printf("Size Of float	:	%lu\n",sizeof(float));
	printf("Size Of double	:	%lu\n",sizeof(double));
	return 0;
}
