#ifndef POINT_H_
#define POINT_H_

namespace pointclass
{
	class Point
	{
	private:
		int xPosition;
		int yPosition;
	public:
		Point( void );
		Point( const int , const int );
		int getXPosition( void ) const;
		void setXPosition( const int );
		int getYPosition( void ) const;
		void setYPosition(const int );
	};
}
using namespace pointclass;
void acceptRecord(Point*,int *,int *);
void printRecord(Point*);
void modifyPoint(Point *);


#endif /* POINT_H_ */
