#include<iostream>
#include "../include/Point.h"
using namespace pointclass;using namespace std;

Point :: Point (void):xPosition(0),yPosition(0)
{	}

Point :: Point(const int xPosition , const int yPosition):xPosition(xPosition),yPosition(yPosition)
{	}

int Point :: getXPosition(void)const
{
	return this->xPosition;
}

void Point :: setXPosition(const int xPosition)
{
	this->xPosition=xPosition;
}

int Point :: getYPosition(void)const
{
	return this->yPosition;
}

void Point :: setYPosition(const int yPosition)
{
	this->yPosition=yPosition;
}

void acceptRecord(Point*p,int *x,int *y)
{
	cout << "Enter X Co-ordinate	:	";
	cin >> *x;
	p->setXPosition(*x);
	cout << "Enter Y Co-ordinate	:	";
	cin >> *y;
	p->setYPosition(*y);
	cout << endl;
}

void printRecord(Point*p)
{
	int x=p->getXPosition();
	cout << "X co-ordinate		:	" << x << endl;
	int y=p->getYPosition();
	cout << "Y co-ordinate		:	" << y << endl << endl;
}

void modifyPoint(Point *p)
{
	int x,y;
	cout << "Enter New Value Of X Co-ordinate	:	";
	cin >> x;
	p->setXPosition(x);
	cout << "Enter New Value Of Y Co-ordinate	:	";
	cin >> y;
	p->setYPosition(y);
	cout << endl;
}
