#include<iostream>
#include "../include/Point.h"
using namespace pointclass;
using namespace std;

int menulist()
{
	int choice;
	cout << "0.Exit" << endl;
	cout << "1.Enter Point Co-ordinates" << endl;
	cout << "2.Display Point Co-ordinates" << endl;
	cout << "3.Modify Point Co-ordinates" << endl << endl;
	cout << "Enter Your Choice	:	";
	cin >> choice;
	cout << endl;
	return choice;
}


int main()
{
	int choice,x,y;
	Point p;
	while((choice=menulist())!=0)
	{
		switch(choice)
		{
		case 1:
			acceptRecord(&p,&x,&y);
			break;
		case 2:
			printRecord(&p);
			break;
		case 3:
			modifyPoint(&p);
			break;
		}
	}
	return 0;
}
