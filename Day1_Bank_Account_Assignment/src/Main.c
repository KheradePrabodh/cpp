
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int i=5;
int k=-1;
int acc_number=1;

struct Account
{
	char name[ 50 ];
	int number ;
	char type[50];
	float balance;
};

int accept_account_info( struct Account *ptr )
{
	int acc_type;
	char s1[]="Savings";
	char s2[]="Current";
	printf("Account Holder Name	:	");
	scanf("%s",ptr[k].name);
	ptr[k].number=acc_number++;
	printf("Account Type\n1.Savings\n2.Current	:	");
	scanf("%d",&acc_type);
	if(acc_type==1)
	{
		strcpy(ptr[k].type,s1);
	}
	else if(acc_type==2)
	{
		strcpy(ptr[k].type,s2);
	}
	printf("Starting Amount to be deposited	:	");
	scanf("%f", &ptr[k].balance);
	return ptr[k].number;
}

void print_account_info( struct Account *ptr)
{
		printf("Account Holder Name	:	%s\n", ptr->name );
		printf("Account Number	:	%d\n", ptr->number );
		printf("Account Type	:	%s\n", ptr->type );
		printf("Account Balance	:	%f\n", ptr->balance );
}

float deposit(struct Account *ptr,float amount)
{
	ptr->balance=ptr->balance+amount;
	return ptr->balance;
}

float withdraw(struct Account *ptr,float amount)
{
		if(amount<ptr->balance)
		{
			ptr->balance=ptr->balance-amount;
		}
		else
		{
			printf("No Enough Balance\n");
		}

	return ptr->balance;
}

int menu_list( void )
{
	int choice;
	printf("0.Exit\n");
	printf("1.Create Account\n");
	printf("2.Print Account Details\n");
	printf("3.Withdraw\n");
	printf("4.Deposit\n");
	printf("Enter choice	:	");
	scanf("%d",&choice);
	return choice;
}

int main( void )
{
	int choice,acc_number;
	float amount,balance;
	struct Account Acc[i];
	while( ( choice = menu_list( ) ) != 0)
	{
		switch( choice )
		{
		case 1:
			k++;
			acc_number=accept_account_info(Acc);
			printf("Your Account number is %d\n",acc_number);
			break;
		case 2:
			printf("Enter Your Account Number	:	");
			scanf("%d",&acc_number);
			for(int j=0;j<i;j++)
			{
				if(Acc[j].number==acc_number)
				{
					print_account_info(&Acc[j]);
					break;
				}
			}
			break;
		case 3:
			printf("Enter Account Number	:	");
			scanf("%d",&acc_number);
			for(int l=0;l<i;l++)
			{
			if(acc_number==Acc[l].number)
			{
				printf("Enter Amount to  Withdraw	:	");
				scanf("%f",&amount);
				balance=withdraw(&Acc[l],amount);
				printf("Available Balance	:	%f\n",balance);
				break;
			}
			}
			break;
		case 4:
			printf("Enter Account Number	:	");
			scanf("%d",&acc_number);
			for(int p=0;p<i;p++)
			{
			if(acc_number==Acc[p].number)
			{
			printf("Enter Amount to be deposited	:	");
			scanf("%f",&amount);
			balance=deposit(&Acc[p],amount);
			printf("%f Deposited Successfully\nAvailable Balance	:	%f\n",amount,balance);
			break;
			}
			}
			break;
		}
	}
	return 0;
}
