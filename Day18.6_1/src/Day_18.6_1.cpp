#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( int real = 0, int imag = 0 ):real(real),imag(imag)
	{	}
	void printRecord( void )
	{
		cout<<this->real<<"	"<<this->imag<<endl;
	}
	friend Complex operator+( Complex &c1, Complex &c2 )
	{
		Complex temp;
		temp.real = c1.real + c2.real;
		temp.imag = c1.imag + c2.imag;
		return temp;
	}
	friend Complex operator-( Complex &c1, Complex &c2 )
	{
		Complex temp;
		temp.real = c1.real - c2.real;
		temp.imag = c1.imag - c2.imag;
		return temp;
	}
	friend Complex operator*( Complex &c1, Complex &c2 )
	{
		Complex temp;
		temp.real = c1.real * c2.real;
		temp.imag = c1.imag * c2.imag;
		return temp;
	}
	friend Complex operator/( Complex &c1, Complex &c2 )
	{
		Complex temp;
		temp.real = c1.real / c2.real;
		temp.imag = c1.imag / c2.imag;
		return temp;
	}
	friend Complex operator%( Complex &c1, Complex &c2 )
	{
		Complex temp;
		temp.real = c1.real % c2.real;
		temp.imag = c1.imag % c2.imag;
		return temp;
	}

};

int main( void )
{
	Complex c1( 10, 20 );
	Complex c2( 30, 40 );
	cout << "c1	:	";
	c1.printRecord();
	cout << "c2	:	";
	c2.printRecord();
	cout << endl;

	Complex c3;

	c3 = c1 + c2;	//c3 = operator+( c1, c2 );
	cout << "c1+c2	:	";
	c3.printRecord();

	c3 = c1 - c2;	//c3 = operator-( c1, c2 );
	cout << "c1-c2	:	";
	c3.printRecord();

	c3 = c1 * c2;	//c3 = operator*( c1, c2 );
	cout << "c1*c2	:	";
	c3.printRecord();

	c3 = c1 / c2;	//c3 = operator/( c1, c2 );
	cout << "c1/c2	:	";
	c3.printRecord();

	c3 = c1 % c2;	//c3 = operator%( c1, c2 );
	cout << "c1%c2	:	";
	c3.printRecord();

	return 0;
}
