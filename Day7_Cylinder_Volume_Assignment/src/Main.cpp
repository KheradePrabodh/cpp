#include<iostream>
using namespace std;

namespace cylinder
{
	class Cylinder
	{
	private:
		double height;
		double radius;
		double volume;

	public:
		void initCylinder();
		void acceptRecord();
		void calculateVolume();
		void printRecord();
	};
}

using namespace cylinder;
void Cylinder::initCylinder()
{
	height=0.0;
	radius=0.0;
	volume=0.0;
}

void Cylinder::acceptRecord()
{
	cout << "Enter Height Of Cylinder	:	";
	cin >> height;
	cout << "Enter Radius Of Cylinder	:	";
	cin >> radius;
}

void Cylinder::calculateVolume()
{
	volume=3.14 * radius * radius * height;
}

void Cylinder::printRecord()
{
	cout << "Height Of Cylinder Is	:	" << height <<endl;
	cout << "Radius Of Cylinder Is	:	" << radius <<endl;
	cout << "Volume Of Cylinder Is	:	" << volume <<endl;
}

int menulist()
{
	int choice;
	cout << "0.Exit" << endl;
	cout << "1.Accept Record" << endl;
	cout << "2.Print Record" << endl;
	cout << "Enter Your Choice	:	";
	cin >> choice;
	return choice;
}

int main(void)
{
	int choice;
	Cylinder cylinder;
	cylinder.initCylinder();
	while((choice=menulist())!=0)
	{
		switch(choice)
		{
		case 1:
			cylinder.acceptRecord();
			cylinder.calculateVolume();
		    break;
		case 2:
			cylinder.printRecord();
		    break;
		}
	}
	return 0;
}


/*int main(void)
{
	Cylinder cylinder;
	cylinder.initCylinder();

	cylinder.acceptRecord();
	cylinder.calculateVolume();
	cylinder.printRecord();
	return 0;
}*/
