#include<iostream>
using namespace std;
class Rectangle
{
private:
	float area;
	float length;
	float breadth;
public:
	void acceptRecord( void )
	{
		cout<<"Length	:	";
		cin>>this->length;
		cout<<"Breadth	:	";
		cin>>this->breadth;
	}
	void calculateArea( void )
	{
		this->area = this->length * this->breadth;
	}
	void printRecord( void )
	{
		cout<<"Area	:	"<<this->area<<endl;
	}
};
class Math
{
public:
	static const float PI;
public:
	static float pow( float base, int index )
	{
		float result = 1;
		for( int count = 1; count <= index; ++ count )
			result = result * base;
		return result;
	}
};
const float Math::PI=3.142;
class Circle
{
private:
	float area;
	float radius;
public:
	void acceptRecord( void )
	{
		cout<<"Radius	:	";
		cin>>this->radius;
	}
	void calculateArea( void )
	{
		this->area = Math::PI * Math::pow(this->radius,2);
	}
	void printRecord( void )
	{
		cout<<"Area	:	"<<this->area<<endl;
	}
};
int main( void )
{
	Circle c;
	c.acceptRecord();
	c.calculateArea();
	c.printRecord();
	return 0;
}
int main1( void )
{
	Rectangle rect;
	rect.acceptRecord( );
	rect.calculateArea( );
	rect.printRecord( );
	return 0;
}
