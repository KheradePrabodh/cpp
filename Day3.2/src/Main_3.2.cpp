#include<stdio.h>

struct Employee
{
private:
	char name[30];
	int empid;
	float salary;
public:
	void acceptRecord( void )
	{
		printf("Name	:	");
		scanf("%[^\n]%*c", name);
		printf("Empid	:	");
		scanf("%d", &empid);
		printf("Salary	:	");
		scanf("%f", &salary);
	}
	void printRecord( void )
	{
		printf("Name	:	%s\n", name);
		printf("Empid	:	%d\n", empid);
		printf("Salary	:	%.2f\n", salary);
	}
};

int main( void )
{
	Employee emp;
	emp.acceptRecord( ); //emp.acceptRecord(&emp);
	//emp.salary=-1000; //not allowed
	emp.printRecord( ); //emp.printRecord(&emp);
	return 0;
}

/*struct Employee
{

	char name[30];
	int empid;
	float salary;

	void acceptRecord( void )
	{
		printf("Name	:	");
		scanf("%[^\n]%*c", name);
		printf("Empid	:	");
		scanf("%d", &empid);
		printf("Salary	:	");
		scanf("%f", &salary);
	}
	void printRecord( void )
	{
		printf("Name	:	%s\n", name);
		printf("Empid	:	%d\n", empid);
		printf("Salary	:	%.2f\n", salary);
	}
};
	int main( void )
	{
		Employee emp;
		emp.acceptRecord( );
		//emp.salary=-1000; //allowed
		emp.printRecord( );
		return 0;
	}*/
