#include<iostream>
using namespace std;

#define MAX 20
namespace stack
{
	class StackOverflowException
	{
	private:
		string message;
	public:
		StackOverflowException(string message)throw():message(message)
		{	}
		const string& getMessage()const throw()
		{
			return message;
		}
	};
	class StackUnderflowException
	{
	private:
		string message;
	public:
		StackUnderflowException(string message)throw():message(message)
		{	}
		const string& getMessage()const throw()
		{
			return message;
		}
	};
	class Stack
	{
	private:
		int arr[MAX];
		int top;

	public:
		Stack(void);
		bool isFull(void);
		void pushElement(void);
		bool isEmpty();
		int popElement();
		int peekElement();
	};
}

void acceptElement(int*);

using namespace stack;
Stack::Stack(void)
{
	this->top=-1;
}

bool Stack:: isFull(void)
{
	if (this->top==MAX-1)
		return true;
	else
		return false;
}

void Stack::pushElement()
{
	if(isFull())
		throw StackOverflowException("Stack Is Full");
	int  element;
	acceptElement(&element);
	++(this->top);
	this->arr[this->top]=element;
}

bool Stack:: isEmpty(void)
{
	if(this->top==-1)
		return true;
	else
		return false;
}

int Stack:: popElement(void)
{
	if(isEmpty())
		throw StackUnderflowException("Stack Is Empty");
	int popped_element=this->arr[this->top];
	--(this->top);
	return popped_element;
}

int Stack:: peekElement(void)
{
	if(isEmpty())
		throw StackUnderflowException("Stack Is Empty");
	int peeked_element=this->arr[top];
	return peeked_element;
}

void acceptElement(int*ptrElement)
{
	cout<< "Enter Element to push	:	";
	cin >> *ptrElement;
	cout << endl;
}


int menulist()
{
	int choice;
	cout << "0.Exit" << endl;
	cout << "1.PUSH Element" << endl;
	cout << "2.POP Element" << endl;
	cout << "3.PEEK Element" << endl << endl;
	cout << "Enter your choice	:	";
	cin >> choice;
	return choice;
}

int main()
{
	Stack S;
	int choice;
	while((choice=menulist())!=0)
	{
		try
		{
			switch(choice)
			{
			case 1:
				S.pushElement();
				break;
			case 2:
				int popped_element;
				popped_element=S.popElement();
				cout << "Popped Element is	:	" << popped_element << endl << endl;
				break;
			case 3:
				int peeked_element=S.peekElement();
				cout << "Peeked Element		:	" << peeked_element << endl << endl;
				break;
			}
		}
		catch(StackOverflowException &ex)
		{
			cout << ex.getMessage() << endl;
			cout << endl;
		}
		catch(StackUnderflowException &ex)
		{
			cout << ex.getMessage() << endl;
			cout << endl;
		}
		catch(...)
		{
			cout << "Exception" << endl;
			cout << endl;
		}
	}
	return 0;
}
