#include<iostream>
using namespace std;

#include "../include/Cylinder.h"
using namespace cylinder;

int menulist()
{
	int choice;
	cout << "0.Exit" << endl;
	cout << "1.Accept Record" << endl;
	cout << "2.Print Record" << endl;
	cout << "Enter Your Choice	:	";
	cin >> choice;
	return choice;
}

int main(void)
{
	int choice;
	Cylinder cylinder;
	cylinder.initCylinder();
	while((choice=menulist())!=0)
	{
		switch(choice)
		{
		case 1:
			cylinder.acceptRecord();
			cylinder.calculateVolume();
			break;
		case 2:
			cylinder.printRecord();
			break;
		}
	}
	return 0;
}
