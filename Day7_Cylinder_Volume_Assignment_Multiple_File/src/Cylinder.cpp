#include<iostream>
using namespace std;


#include "../include/Cylinder.h"
using namespace cylinder;

void Cylinder:: initCylinder()
{
	height=0.0;
	radius=0.0;
	volume=0.0;
}

void Cylinder::acceptRecord()
{
	cout << "Enter Height Of Cylinder	:	";
	cin >> height;
	cout << "Enter Radius Of Cylinder	:	";
	cin >> radius;
}
void Cylinder::calculateVolume()
{
	volume=3.14 * radius * radius * height;
}
void Cylinder::printRecord()
{
	cout << "Height Of Cylinder Is	:	" << height <<endl;
	cout << "Radius Of Cylinder Is	:	" << radius <<endl;
	cout << "Volume Of Cylinder Is	:	" << volume <<endl;
}
