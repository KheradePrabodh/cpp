#ifndef CYLINDER_H_
#define CYLINDER_H_

namespace cylinder
{
	class Cylinder
	{
	private:
		double height;
		double radius;
		double volume;

	public:
		void initCylinder();
		void acceptRecord();
		void calculateVolume();
		void printRecord();
	};
}

#endif /* CYLINDER_H_ */
