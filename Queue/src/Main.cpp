#include<iostream>
#include<cstring>
#include<cstdlib>
using namespace std;

class Exception
{
	string message;
public:
	Exception (const string message)throw():message(message)
{}

	const string& getMessage() const {
		return message;
	}
};
class Queue
{
private:
	int size;
	int *arr;
	int front;
	int rear;

public:
	Queue():size(0),arr(NULL),front(-1),rear(-1)
{	}
	Queue(int size):size(size),front(-1),rear(-1)
	{
		this->arr = new int[size * sizeof(int)];
		memset(this->arr , -1 , sizeof(int)*size );
	}
	void pushElement(int number)throw(Exception)
	{
		if(this->front < (this->size-1))
		{
			if( this->front == -1 ){ this->rear = 0;}
			++this->front;
			this->arr[this->front] = number;
		}
		else
			throw Exception("Queue is Full!!!");
	}

	int getElement(void)throw(Exception)
	{
		if(this->rear <= this->front)
		{
			return this->arr[this->rear];
		}
		else
			throw Exception("Queue is Empty!!!");
	}

	void popElement()throw(Exception)
	{
		if(this->rear <= this->front)
		{
			this->arr[this->rear] = -1;
			++this->rear;
		}
		else
			throw Exception("Queue is Empty!!!");
	}
	~Queue()
	{
		delete[] this->arr;
	}
};

int menu_list( void )
{
	int choice;
	cout<<endl<<"0.Exit"<<"\t";
	cout<<"1.Push"<<"\t";
	cout<<"2.Pop"<<"\t";
	cout<<"3.Peek"<<endl;
	cout<<"Enter your choice	:	";
	cin>>choice;
	return choice;
}

int main()
{
	int size=1;
	cout<<"Enter Queue Size	:	";
	cin>>size;
	Queue s1(size);
	int number,choice;
	while((choice=menu_list())!=0)
	{
		try
		{
			switch(choice)
			{
			case 1:
				cout<<"Enter element to push	:	";
				cin>>number;
				s1.pushElement(number);
				break;
			case 2:
				cout<<"Pop element	:	"<<s1.getElement()<<endl;
				s1.popElement();
				break;
			case 3:
				cout<<"Top element	:	"<<s1.getElement()<<endl;
				break;
			}
		}
		catch(Exception &ex)
		{
			cout<<ex.getMessage()<<endl;
		}
	}
	return 0;
}
