#include<iostream>

#include "../include/Account.h"
#include "../include/Bank.h"

using namespace std;

int count = 1000;
using namespace bank;
	void Bank:: init()
	{
		index=-1;
	}
	void Bank:: accept_account_info( Account *ptraccount )
	{
		cout << endl;
		cout << "Name	:	";
		cin >> ptraccount->name ;
		cout << "Type	:	";
		cin >> ptraccount->type ;
		cout << "Balance	:	";
		cin>> ptraccount->balance ;
	}

	int Bank:: create_account(   Account account )
	{
		account.number = ++ count;
		arr[ ++index ] = account;
		return account.number;
	}

	float Bank:: deposit( int number, float amount )
	{
		for( int i = 0; i <= index; ++i )
		{
			if( arr[ i ].number == number  )
			{
				arr[ i ].balance += amount;
				return arr[ i ].balance;
			}
		}
		return 0;
	}

	float Bank:: withdraw( int number, float amount )
	{
		for( int i = 0; i <= index; ++ i )
		{
			if( arr[ i ].number == number  )
			{
				arr[ i ].balance -= amount;
				return arr[ i ].balance;
			}
		}
		return 0;
	}

	Account Bank:: get_account_details( int number )
	{
		Account account;
		for( int i = 0; i <= index; ++ i )
		{
			if( arr[ index ].number == number  )
			{
				account = arr[ index ];
			}
		}
		return account;
	}

	void Bank:: print_account_info( Account *ptraccount )
	{
		cout << endl;
		cout << "Account Holder Name	:	" << ptraccount->name << endl;
		cout << "Account Number		:	" << ptraccount->number << endl;
		cout << "Account Type		:	" << ptraccount->type << endl;
		cout << "Account Balance		:	" << ptraccount->balance << endl;
		cout << endl;
	}

	void Bank:: accept_account_number( int *number )
	{
		cout << "Account number	:	";
		cin >> *number ;
	}

	void Bank:: accept_amount( float *amount )
	{
		cout << "Amount	:	";
		cin >> *amount ;
	}

	void Bank:: print_account_number( int number )
	{
		cout << "Account number	:	" << number << endl;
	}

	void Bank:: print_balance( float balance )
	{
		cout << "Balance	:	"<< balance << endl;
	}
