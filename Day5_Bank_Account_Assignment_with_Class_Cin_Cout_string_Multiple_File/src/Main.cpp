#include<iostream>
using namespace std;

#include "../include/Account.h"
#include "../include/Bank.h"
using namespace bank;

int menu_list( void )
{
	int choice;
	cout << endl;
	cout << "0.Exit" << endl;
	cout << "1.Create New Account" << endl;
	cout << "2.Deposit" << endl;
	cout << "3.Withdraw" << endl;
	cout << "4.Print Account details" << endl;
	cout << endl;
	cout << "Enter choice	:	";
	cin >> choice;
	return choice;
}

int main( void )
{
	int choice, accNumber;
	float balance, amount;
	Bank bank ;
	bank.init();
	Account account;
	while( ( choice = menu_list( ) ) )
	{
		switch( choice )
		{
		case 1:
			bank.accept_account_info(&account);
			accNumber = bank.create_account( account);
			bank.print_account_number(accNumber);
			break;
		case 2:
			bank.accept_account_number(&accNumber);
			bank.accept_amount( &amount );
			balance = bank.deposit( accNumber, amount );
			bank.print_balance(balance);
			break;
		case 3:
			bank.accept_account_number(&accNumber);
			bank.accept_amount(&amount );
			balance = bank.withdraw( accNumber, amount );
			bank.print_balance(balance);
			break;
		case 4:
			bank.accept_account_number(&accNumber);
			account = bank.get_account_details( accNumber);
			bank.print_account_info(&account);
			break;
		}
	}
	return 0;
}
