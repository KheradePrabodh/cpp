#ifndef BANK_H_
#define BANK_H_

#include "../include/Account.h"
using namespace account;

namespace bank
{
	class Bank
	{
	private:
		int index;
		Account arr[ 5 ];
	public:
		void init();
		void accept_account_info( Account * );
		int create_account(   Account );
		float deposit( int , float );
		float withdraw( int , float );
		Account get_account_details( int );
		void print_account_info( Account * );
		void accept_account_number( int * );
		void accept_amount( float * );
		void print_account_number( int );
		void print_balance( float );
	};
}

#endif /* BANK_H_ */
