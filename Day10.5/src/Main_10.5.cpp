#include<iostream>
using namespace std;

int main( void )
{
	int number = 10;
	number = number + 5;
	cout<<"Number	:	"<<number<<endl;	//	ok	//	Number	:	15
	return 0;
}

/*int main( void )
{
	const int number = 10;
	//number = number + 5;
	cout<<"Number	:	"<<number<<endl;	//	ok	//	Number	:	15
	return 0;
}*/

/*int main( void )
{
	const int number = 10;
	number = number + 5;	//	not ok	//	error: assignment of read-only variable ‘number’
	cout<<"Number	:	"<<number<<endl;
	return 0;
}*/

/*int main( void )
{
	const int number;	//	ok	//	error: uninitialized const ‘number’	//	initialization is mandatory
	cout<<"Number	:	"<<number<<endl;
	return 0;
}*/
