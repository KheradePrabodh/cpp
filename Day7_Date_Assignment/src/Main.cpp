#include<iostream>
#include<string>

using namespace std;
namespace date
{
	class Date
	{
	private:
		int day;
		int month;
		int year;
	public:
		void initDate();
		void acceptRecord();
		void printRecord();
		void addNumberOfDays( int );
		string dayOfWeek(void 	);
	};
}

using namespace date;
void Date :: initDate()
	{
		day=0;
		month=0;
		year=0;
	}

void Date :: acceptRecord()
{
	cout << "Enter Day	:	" ;
	cin >> day;
	cout << "Enter Month	:	" ;
	cin >> month;
	cout << "Enter Year	:	" ;
	cin >> year;
}

void Date :: printRecord()
{
	cout << day;
	cout << " / " << month;
	cout << " / " << year << endl;
}

void Date :: addNumberOfDays ( int count )
{
	int day_old=day;
	int total_days=day+count;
	if(month==1||month==3||month==5||month==7||month==8||month==10||month==12)
	{
		if(total_days>31)
		{
			day=(count-(31-day_old));
			month+=1;
		}
		else if(total_days<=31)
		{
			day=total_days;
		}
	}
	else if(month==4||month==6||month==9||month==11)
	{
		if(total_days>30)
		{
			day=(count-(30-day_old));
			month+=1;
		}
		else if(total_days<=30)
		{
			day=total_days;
		}
	}
	else if(month==2)
	{
		if((year%400==0)||((year%4==0)&&(year%100!=0)))
		{
			if(total_days>29)
			{
				day=(count-(29-day_old));
				month+=1;
			}
			else if(total_days<=29)
			{
				day=total_days;
			}

		}
		else
		{
			if(total_days>28)
			{
				day=(count-(28-day_old));
				month+=1;
			}
			else if(total_days<=28)
			{
				day=total_days;
			}
		}
	}
}

string Date :: dayOfWeek()
{
	string days[7]={"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
	return days[day];
}

int menulist()
{
	int choice;
	cout << "0.Exit" << endl;
	cout << "1.Accept Record" << endl;
	cout << "2.Add Number Of Days" << endl;
	cout << "3.Print Record" << endl;
	cout << "Enter Your Choice	:	";
	cin >> choice;
	return choice;
}

int main(void)
{
	int choice,days;
	Date date;
	date.initDate();
	while((choice=menulist())!=0)
	{
		switch(choice)
		{
		case 1:
			date.acceptRecord();
			break;
		case 2:
			cout << "Enter Number Of Days To Be Added	:	";
			cin >>days;
			date.addNumberOfDays(days);
			break;
		case 3:
			date.printRecord();

		}
	}
}

/*int main( void )
{
	int days;

	Date date;
	date.initDate();

	date.acceptRecord();
	date.printRecord();

	cout << "Enter Number Of Days To Be Added	:	";
	cin >>days;

	date.addNumberOfDays(days);
	date.printRecord();
	return 0;
}*/
