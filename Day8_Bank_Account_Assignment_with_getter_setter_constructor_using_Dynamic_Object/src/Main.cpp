#include<iostream>
#include<string>

using namespace std;

namespace account
{
	class Account
	{
	private:
		string name;
		int number;
		string type;
		float balance;
	public:
		string& getName()
		{
			return this->name;
		}
		void setName(string &name)
		{
			this->name = name;
		}


		int getNumber()
		{
			return this->number;
		}
		void setNumber(int number)
		{
			this->number = number;
		}


		float getBalance()
		{
			return this->balance;
		}
		void setBalance(float balance)
		{
			this->balance = balance;
		}


		string& getType()
		{
			return type;
		}
		void setType(string &type)
		{
			this->type = type;
		}
	};
}

int count = 1000;

using namespace account;
namespace bank
{
	class Bank
	{
	private:
		int index;
		Account *arr[ 5 ];
	public:

		void accept_account_info( Account * );
		int create_account(  Account* );
		float deposit( int , float );
		float withdraw( int number, float );
		Account* get_account_details( int );
		void print_account_info( Account * );
		void accept_account_number( int * );
		void accept_amount( float * );
		void print_account_number( int );
		void print_balance( float );
	};
}

using namespace bank;

void Bank :: accept_account_info( Account *ptraccount )
{
	string name,type;
	float balance;
	cout << endl;
	cout << "Name	:	";
	cin >> name ;
	ptraccount->setName(name);
	cout << "Type	:	";
	cin >>type;
	ptraccount->setType(type);
	cout << "Balance	:	";
	cin>>balance;
	ptraccount->setBalance(balance);
}

int Bank :: create_account(  Account *ptraccount )
{
	ptraccount->setNumber(++ count);
	this->arr[++this->index] = ptraccount;
	return ptraccount->getNumber();
}

float Bank :: deposit( int number, float amount )
{
	for( int index = 0; index <= this->index; ++ index )
	{
		if( this->arr[index]->getNumber() == number  )
		{
			this->arr[index]->setBalance(this->arr[index]->getBalance()+amount);
			return this->arr[index]->getBalance();
		}
	}
	return 0;
}

float Bank :: withdraw( int number, float amount )
{
	for( int index = 0; index <= this->index; ++ index )
	{

			if( this->arr[index]->getNumber() == number  )
			{
				this->arr[index]->setBalance(this->arr[index]->getBalance()-amount);
				return this->arr[index]->getBalance();
			}
	}
	return 0;
}

Account* Bank :: get_account_details( int number )
{
	Account *ptraccount=(Account *) malloc(sizeof(Account));
	for( int index = 0; index <= this->index; ++ index )
	{
		if( this->arr[index]->getNumber() == number  )
		{
			ptraccount = this->arr[index];
		}
	}
	return ptraccount;
}

void Bank :: print_account_info( Account *ptraccount )
{
	cout << endl;
	cout << "Account Holder Name	:	" << ptraccount->getName() << endl;
	cout << "Account Number		:	" << ptraccount->getNumber() << endl;
	cout << "Account Type		:	" << ptraccount->getType()<< endl;
	cout << "Account Balance		:	" << ptraccount->getBalance() << endl;
	cout << endl;
}

void Bank :: accept_account_number( int *number )
{
	cout << "Account number	:	";
	cin >> *number ;
}

void Bank :: accept_amount( float *amount )
{
	cout << "Amount	:	";
	cin >> *amount ;
}

void Bank :: print_account_number( int number )
{
	cout << "Account number	:	" << number << endl;
}

void Bank :: print_balance( float balance )
{
	cout << "Balance	:	"<< balance << endl;
}
int menu_list( void )
{
	int choice;
	cout << endl;
	cout << "0.Exit" << endl;
	cout << "1.Create New Account" << endl;
	cout << "2.Deposit" << endl;
	cout << "3.Withdraw" << endl;
	cout << "4.Print Account details" << endl;
	cout << endl;
	cout << "Enter choice	:	";
	cin >> choice;
	return choice;
}

int main( void )
{
	int choice, accNumber;
	float balance, amount;
	Bank *ptrbank=(Bank *) malloc(sizeof(Bank));
	Account *ptraccount=(Account *) malloc(sizeof(Account));
	while( ( choice = menu_list( ) ) )
	{
		switch( choice )
		{
		case 1:
			ptrbank->accept_account_info(ptraccount);
			accNumber = ptrbank->create_account(ptraccount);
			ptrbank->print_account_number(accNumber);
			break;
		case 2:
			ptrbank->accept_account_number(&accNumber);
			ptrbank->accept_amount( &amount );
			balance = ptrbank->deposit( accNumber, amount );
			ptrbank->print_balance(balance);
			break;
		case 3:
			ptrbank->accept_account_number(&accNumber);
			ptrbank->accept_amount(&amount );
			balance = ptrbank->withdraw( accNumber, amount );
			ptrbank->print_balance(balance);
			break;
		case 4:
			ptrbank->accept_account_number(&accNumber);
			ptraccount = ptrbank->get_account_details( accNumber);
			ptrbank->print_account_info(ptraccount);
			break;
		}
	}
	return 0;
}
