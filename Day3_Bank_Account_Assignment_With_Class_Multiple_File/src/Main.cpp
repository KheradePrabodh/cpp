#include<stdio.h>

#include "../include/Account.h"
#include "../include/Bank.h"


int menu_list( void )
{
	int choice;
	printf("0.Exit\n");
	printf("1.Create New Account\n");
	printf("2.Deposit\n");
	printf("3.Withdraw\n");
	printf("4.Print Account details\n");
	printf("Enter choice	:	");
	scanf("%d",&choice);
	return choice;
}

int main( void )
{
	int choice, accNumber;
	float balance, amount;
	Bank bank;
	bank.init();
	Account account;
	while( ( choice = menu_list( ) ) )
	{
		switch( choice )
		{
		case 1:
			bank.accept_account_info(&account);
			accNumber = bank.create_account( account);
			bank.print_account_number(accNumber);
			break;
		case 2:
			bank.accept_account_number(&accNumber);
			bank.accept_amount( &amount );
			balance = bank.deposit( accNumber, amount );
			bank.print_balance(balance);
			break;
		case 3:
			bank.accept_account_number(&accNumber);
			bank.accept_amount(&amount );
			balance = bank.withdraw( accNumber, amount );
			bank.print_balance(balance);
			break;
		case 4:
			bank.accept_account_number(&accNumber);
			account = bank.get_account_details( accNumber);
			bank.print_account_info(&account);
			break;
		}
	}
	return 0;
}
