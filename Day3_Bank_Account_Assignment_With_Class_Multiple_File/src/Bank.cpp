#include <stdio.h>
#include<stdlib.h>

#include "../include/Account.h"
#include "../include/Bank.h"

int count = 1000;

void Bank :: init(void)
{
	index=-1;
}

void Bank :: accept_account_info( Account *ptraccount )
{
	printf("Name	:	");
	scanf("%s", ptraccount->name );
	printf("Type	:	");
	scanf("%s", ptraccount->type );
	printf("Balance	:	");
	scanf("%f", &ptraccount->balance );
}

int Bank :: create_account( Account account )
{
	account.number = ++ count;
	arr[ ++index ] = account;
	return account.number;
}

float Bank :: deposit( int number, float amount )
{
	for( int i = 0; i <= index; ++ i )
	{
		if( arr[ i ].number == number  )
		{
			arr[ i ].balance += amount;
			return arr[ i ].balance;
		}
	}
	return 0;
}

float Bank :: withdraw( int number, float amount )
{
	for( int i = 0; i <= index; ++ i )
	{
		if( arr[ i ].number == number  )
		{
			arr[ i ].balance -= amount;
			return arr[ i ].balance;
		}
	}
	return 0;
}

Account Bank :: get_account_details( int number )
{
       struct Account account;
	for( int i = 0; i <= index; ++ i )
	{
		if( arr[ i ].number == number  )
		{
			account = arr[ i ];
		}
	}
	return account;
}

void Bank :: print_account_info( Account *ptraccount )
{
	printf("%-20s%-5d%-10s%-10.2f\n", ptraccount->name, ptraccount->number, ptraccount->type, ptraccount->balance);
}

void Bank :: accept_account_number( int *number )
{
	printf("Account number	:	");
	scanf("%d", number );
}

void Bank :: accept_amount( float *amount )
{
	printf("Amount	:	");
	scanf("%f", amount );
}

void Bank :: print_account_number( int number )
{
	printf("Account number	:	%d\n", number);
}

void Bank :: print_balance( float balance )
{
	printf("Balance	:	%f\n", balance );
}
