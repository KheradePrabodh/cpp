#include<iostream>
#include<string>
#pragma pack(1)
using namespace std;

namespace kdac
{
	class Employee
	{
	private:
		string name;
		int empid;
		float salary;
	public:
		void initEmployee( void )
		{
			name = "";
			empid = 0;
			salary = 0;
		}
		void acceptRecord( void )
		{
			cout<<"Name	:	";
			cin>> name;
			cout<<"Empid	:	";
			cin>>empid;
			cout<<"Salary	:	";
			cin>>salary;
		}
		void printRecord( void )
		{
			cout<<"Name	:	"<<name<<endl;
			cout<<"Empid	:	"<<empid<<endl;
			cout<<"Salary	:	"<<salary<<endl;
		}
	};
}
int main( void )
{
	using namespace kdac;
	Employee emp;
	emp.initEmployee( );
	emp.acceptRecord( );
	emp.printRecord( );
	return 0;
}

/*int main (void)
{
	string s1;
	string s2("Sunbeam");
	cout << "s2	:	" << s2 << endl;
	string s3 = ("Prabodh Kherade");
	cout << "s3	:	" << s3 << endl;
	s2 = s2 + " " + "Karad" ;
	cout << "s2	:	" << s2 << endl;
	return 0;
}*/

/*int main(void)
{
	cout << sizeof(string)<<endl; //32 bytes
	return 0;
}*/

