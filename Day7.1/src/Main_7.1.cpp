#include<iostream>
using namespace std;

void sum( int num1, int num2 )
{
	int result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;	//	Result	:	300
}

int main( void )
{
	sum( 100, 200 );
	return 0;
}


/*void sum( int num1, int num2 )
{
	int result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;	//	Result	:	300
}
void add( double num1, double num2 )
{
	double result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;	//	Result	:	31
}
void addition( int num1, float num2, double num3 )
{
	double result = num1 + num2 + num3;
	cout<<"Result	:	"<<result<<endl;	//	Result	:	60.3
}
int main( void )
{
	sum( 100, 200 );
	add( 10.5, 20.5 );
	addition(10, 20.1f, 30.2);
	return 0;
}*/


/*void sum( int num1, int num2)
{
	int result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;	//	Result	:	30
}
void sum( int num1, int num2, int num3 )
{
	int result = num1 + num2 + num3;
	cout<<"Result	:	"<<result<<endl;	//	Result	:	60
}

int main( void )
{
	sum( 10, 20 );
	sum( 10, 20, 30 );
	return 0;
}*/


/*void sum( int num1, int num2)
{
	int result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;
}
void sum( int num1, int num2 ) // error: redefinition of ‘void sum(int, int)’
{
	int result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;
}

int main( void )
{
	sum( 10, 20 );
	return 0;
}*/


/*void sum( int num1, int num2)
{
	int result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;
}
void sum( int num1, double num2 )
{
	double result = num1  + num2;
	cout<<"Result	:	"<<result<<endl;
}

int main( void )
{
	sum( 10, 20 );	//	Result	:	30
	sum( 10, 20.5 );	//	Result	:	30.5
	return 0;
}*/


/*void sum( int num1, float num2)
{
	int result = num1 + num2;
	cout<<"Result	:	"<<result<<endl;	//	Result	:	30
}
void sum( float num1, int num2 )
{
	double result = num1  + num2;
	cout<<"Result	:	"<<result<<endl;	//	Result	:	30.1
}

int main( void )
{
	sum( 10, 20.1f );
	sum( 10.1f, 20 );
	return 0;
}*/


/*int sum( int num1, int num2)
{
	int result = num1 + num2;
	return result;
}
int main( void )
{
	int result = sum( 10, 20 );
	cout<<"Result : "<<result<<endl;

	return 0;
}*/


/*int sum( int num1, int num2)
{
	int result = num1 + num2;
	return result;
}

int main( void )
{
	int result = sum( 10, 20 );
	cout<<"Result : "<<result<<endl;	//	Result	:	30

	sum(50, 60 );
	return 0;
}*/


/*int sum( int num1, int num2)
{
	int result = num1 + num2;
	return result;
}
void sum( int num, int num2 )	//	error: ambiguating new declaration of ‘void sum(int, int)’
{
	int result = num1 + num2;
	cout<<"Result : "<<result<<endl;
}

int main( void )
{
	int result = sum( 10, 20 );
	cout<<"Result : "<<result<<endl;

	sum(50, 60 );
	return 0;
}*/


/*int main( void )
{
	return 0;
}
int main( int argc, char *argv[] )	//	error: conflicting declaration of C function ‘int main(int, char**)’
{
	return 0;
}*/
