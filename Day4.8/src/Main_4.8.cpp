#include<stdio.h>
/*
namespace na
{
	int num1 = 10;
}
void show_record( void )
{
	printf("Num1	:	%d\n", na::num1);
}
void print_record( void )
{
	printf("Num1	:	%d\n", na::num1);
}
void display_record( void )
{
	printf("Num1	:	%d\n", na::num1);
}
int main( void )
{
	::show_record( );//ok //10
	::print_record( );//ok //10
	::display_record( );//ok //10
	return 0;
}*/

/*
namespace na
{
	int num1 = 10;
}
void show_record( void )
{
	using namespace na;
	printf("Num1	:	%d\n", num1);//ok //10
}
void print_record( void )
{
	using namespace na;
	printf("Num1	:	%d\n", num1);//ok //10
}
void display_record( void )
{
	using namespace na;
	printf("Num1	:	%d\n", num1);//ok //10
}
int main( void )
{
	::show_record( );
	::print_record( );
	::display_record( );
	return 0;
}*/

/*
namespace na
{
	int num1 = 10;
}
using namespace na;
void show_record( void )
{
	printf("Num1	:	%d\n", num1);//ok //10
}
void print_record( void )
{
	printf("Num1	:	%d\n", num1);//ok //10
}
void display_record( void )
{
	printf("Num1	:	%d\n", num1);//ok //10
}
int main( void )
{
	::show_record( );
	::print_record( );
	::display_record( );
	return 0;
}*/

namespace na
{
	int num1 = 10;
}
using namespace na;
void show_record( void )
{
	printf("Num1	:	%d\n", num1);
}
void print_record( void )
{
	printf("Num1	:	%d\n", num1);
}
void display_record( void )
{
	printf("Num1	:	%d\n", num1);
}
int main( void )
{
	na obj; //not allowed //error

	::show_record( );
	::print_record( );
	::display_record( );
	return 0;
}
