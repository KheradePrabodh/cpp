#include<iostream>
using namespace std;

class Outer	//Top Level class
{
private:
	class Inner	//Nested class
	{

	};
	friend int main( void );
};
int main( void )
{
	Outer out;
	Outer::Inner in;	//	ok
	return 0;
}

/*class Outer	//Top Level class
{
private:
	class Inner	//Nested class
	{

	};

};
int main( void )
{
	Outer out;
	Outer::Inner in;	//	error: ‘class Outer::Inner’ is private within this context
	return 0;
}*/
