#include"../include/ShapeFactory.h"
int main( void )
{
	int choice;
	while( ( choice = ShapeFactory::menuList() ) != 0 )
	{
		Shape *ptr =  ShapeFactory::getInstance(choice);
		if( ptr != 0 )
		{
			ptr->acceptRecord( );	//Runtime Polymorphism
			ptr->calculateArea( );	//Runtime Polymorphism
			ptr->printRecord( );
			delete ptr;
		}
	}
	return 0;
}
