#include"../include/Math.h"

const float Math::PI=3.142;

float Math::pow( float base, int index )
{
	float result = 1;
	for( int count = 1; count <= index; ++ count )
		result = result * base;
	return result;
}
