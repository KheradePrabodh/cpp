#ifndef SHAPE_H_
#define SHAPE_H_


class Shape
{
protected:
	float area;
public:
	Shape( void )
	{
		this->area = 0;
	}

	virtual void acceptRecord( void ) = 0;

	virtual float calculateArea( void ) = 0;

	void printRecord( void )const;

	virtual ~Shape( );
};

#endif /* SHAPE_H_ */
