#ifndef SHAPEFACTORY_H_
#define SHAPEFACTORY_H_

#include"../include/Shape.h"

class ShapeFactory
{
public:
	static Shape* getInstance( int choice );

	static int menuList( void );
};

#endif /* SHAPEFACTORY_H_ */
