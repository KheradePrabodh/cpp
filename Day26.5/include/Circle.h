#ifndef CIRCLE_H_
#define CIRCLE_H_

#include"../include/Shape.h"
#include"../include/Math.h"

class Circle : public Shape
{
private:
	float radius;
public:
	Circle( void );

	void acceptRecord( void );

	float calculateArea( void );
};

#endif
