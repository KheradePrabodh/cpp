#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( void )
	{
		this->real = 0;
		this->imag = 0;
	}
	Complex( int real, int imag )
	{
		this->real = real;
		this->imag = imag;
	}
	/*void operator>>(istream &cin)	//	Not recommended
	{
		cout<<"Real Number	:	";
		cin>>this->real;
		cout<<"Imag Number	:	";
		cin>>this->imag;

	}*/

	Complex( const Complex &other )
	{
		cout<<"Complex( const Complex &other )"<<endl;
		this->real = other.real;
		this->imag = other.imag;
	}
	//const Complex &other = c1
	//Complex *const this = &c2
	Complex& operator=( const Complex &other )
	{
		cout<<"void operator=( const Complex &other )"<<endl;
		this->real = other.real;
		this->imag = other.imag;
		return *this;
	}

	friend istream& operator>>( istream &in, Complex &other )
	{
		cout<<"Real Number	:	";
		cin>>other.real;
		cout<<"Imag Number	:	";
		cin>>other.imag;
		return cin;
	}
	friend ostream& operator<<( ostream &cout, const Complex &other )
	{
		cout<<"Real Number	:	"<<other.real<<endl;
		cout<<"Imag Number	:	"<<other.imag<<endl;
		return cout;
	}
};

int main( void )
{
	Complex c1( 100, 200 ),c2,c3;
	c3 = c2 = c1; //c3.operator =( c2.operator =(c1) )
	cout<<c3<<endl;
	return 0;
}
/*int main(void)
{
	Complex c1( 100, 200 );
	Complex c2;
	c2 = c1; //c2.operator=( c1 );
	cout<<c2<<endl;
	return 0;
}*/
