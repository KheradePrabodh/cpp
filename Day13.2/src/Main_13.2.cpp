#include<iostream>
using namespace std;

/*int main(void)
{
	try
	{
		int count=-3;
		int *ptr = new int[count];
	}
	catch(bad_alloc &ex)
	{
		cout << ex.what() << endl;		//	std::bad_array_new_length
	}
	return 0;
}*/



/*int main( void )
{
	char name[ 30 ];
	cout<<"Name	:	";
	cin>>name;
	cout<<"Name	:	"<<name<<endl;

			//	Placement new operator
	//int *ptr = new (&name[6]) int(125);		//	using memory from 6th byte location
 	int *ptr = new ( name) int( 125 );			//	using memory from 0th byte location
	//int *ptr = ( int* ) operator new(sizeof(int), name );		//	internal conversion

	cout<<*ptr<<endl;
	return 0;
}*/



/*int main( void )
{
			//Memory allocation and deallocation for Multidimensional array

	int **ptr = new int*[ 4 ];						//	Memory Allocation
	for( int index = 0; index < 4; ++ index )		//	Memory Allocation
		ptr[ index ] = new int[ 3 ];				//	Memory Allocation

	for( int row = 0; row < 4; ++ row )				//	Accepting record
		{
			for( int col = 0; col < 3; ++ col )
			{
				cout<<"ptr[ "<<row<<" ][ "<<col<<" ]	:	";
				cin>>ptr[ row ][ col ];
			}
		}

		for( int row = 0; row < 4; ++ row )			//	Printing record
		{
			for( int col = 0; col < 3; ++ col )
			{
				cout<<ptr[ row ][ col ]<<"	";
			}
			cout<<endl;
		}


	for( int index = 0; index < 4; ++ index )		//	De-allocating memory
		delete[] ptr[ index ];						//	De-allocating memory
	delete[] ptr;									//	De-allocating memory
	ptr = NULL;
	return 0;
}*/



/*int main( void )
{
	//int *ptr = new int[ 3 ]();
	//int *ptr = new int[ 3 ]{0,0,0};
	int *ptr = new int[ 3 ]{50,60,70};
	//int *ptr = ( int* )operator new[]( 3  * sizeof( int ) );

	//ptr[ 0 ] = 10;
	//ptr[ 1 ] = 20;
	//ptr[ 2 ] = 30;

	for( int index = 0; index < 3; ++ index )
		cout<<ptr[ index ]<<endl;

	delete[] ptr;
	//operator delete[](ptr);

	ptr = NULL;
	return 0;
}*/



/*int main( void )
{
			//Memory allocation and deallocation for 1D array

	int *ptr = new int[ 3 ];									//	Memory allocation
	//int *ptr = ( int* )operator new[]( 3  * sizeof( int ) );	//	Internal conversion

	ptr[ 0 ] = 10;
	ptr[ 1 ] = 20;
	ptr[ 2 ] = 30;

	for( int index = 0; index < 3; ++ index )
		cout<<ptr[ index ]<<endl;

	delete[] ptr;												//	Memory De-allocation
	//operator delete[](ptr);									//	Internal conversion

	ptr = NULL;
	return 0;
}*/



/*int main( void )
{
	int *p1 = new int;	//	Default value =	Garbage value

	int *p2 = new int( );	//	Default value =	0

	int *p3 = new int( 3 );		//	Default value =	3

	return 0;
}*/



/*int main( void )
{
	//Memory allocation and deallocation for single variable

	int *ptr = new int;									//	Memory allocation
	//int *ptr = (int*)operator new(sizeof(int));		//	internal conversion

	*ptr = 125;	//dereferencing
	cout<<"Value	:	"<<*ptr<<endl;					//	Dereferencing

	delete ptr;											//Memory deallocation
	//operator delete(ptr);

	ptr = NULL;
	return 0;
}*/



/*class Test
{

};
int main()
{
	Test t;
	cout << sizeof(t) << endl;
	return 0;
}*/
