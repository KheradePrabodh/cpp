#include<cstring>
#include<iostream>
using namespace std;

namespace NewString
{
class String
{
private:
	int length;
	char *buffer;
public:
	String( void );
	String( char *buffer );
	//void acceptRecord( void );
	//void printRecord( void );
	~String( void );
	char* getBuffer();
	int getLength();
};
}

int buffer_length(char *ptr)
{
	int counter=0;
	while(*ptr != '\0')
	{
		counter++;
		ptr++;
	}
	return counter;
}
using namespace NewString;
String::String( void ) : length( 0 ), buffer( NULL )
{
	cout<<"Array( void )"<<endl;
}
String::String( char *buffer )
{
	int i=0;
	this->length = buffer_length(buffer);
	//this->buffer = new(buffer) char[(sizeof(char) * length)+1];
	this->buffer = new char[(sizeof(char) * length)+1];
	while(buffer[i] != '\0')
	{
		this->buffer[i] = buffer[i];
		i++;
	}
	this->buffer[i]='\0';
}
char* String:: getBuffer()
{
	return buffer;
}

int String:: getLength()
{
	return length;
}
void acceptRecord( char * string )
{
	cout<<"Enter String	:	";
	cin>>string;
	cout<<string;
}
void printRecord( String &s )
{
	cout<<endl<<s.getBuffer()<<"	length	:	"<<s.getLength()<<endl;
}
String::~String( void )
{
	delete[] this->buffer;
}

int main( void )
{
	char string[1024];
	acceptRecord(string);
	String a1(string);
	printRecord( a1 );
	return 0;
}
