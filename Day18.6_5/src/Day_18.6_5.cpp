#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( int real = 0, int imag = 0 )
	{
		this->real = real;
		this->imag = imag;
	}
	void printRecord( void )
	{
		cout<<this->real<<"	"<<this->imag<<endl;
	}
	/////////////////Pre_Increment_Decrement///////////////
	friend Complex operator++( Complex &c1 )
	{
		Complex temp;
		temp.real = ++ c1.real;
		temp.imag = ++ c1.imag;
		return temp;
	}
	friend Complex operator--( Complex &c1 )
	{
		Complex temp;
		temp.real = -- c1.real;
		temp.imag = -- c1.imag;
		return temp;
	}
	/////////////////Post_Increment_Decrement///////////////
	friend Complex operator++( Complex &c1, int )
	{
		Complex temp;
		temp.real = c1.real ++;
		temp.imag = c1.imag ++;
		return temp;
	}
	friend Complex operator--( Complex &c1, int )
	{
		Complex temp;
		temp.real = c1.real --;
		temp.imag = c1.imag --;
		return temp;
	}
};


/////////////////Post_Increment_Decrement///////////////
int main( void )
{
	Complex c1( 10, 20 );
	cout << "c1	:	";
	c1.printRecord();
	cout << endl;

	Complex c2;

	cout << "Post_Increment" << endl;
	c2 = c1++;	//c2 = operator++( c1,0 );
	cout << "c1	:	";
	c1.printRecord();
	cout << "c2	:	";
	c2.printRecord();

	cout << "Post_Decrement" << endl;
	c2 =  c1--;	//c2 = operator--( c1,0 );
	cout << "c1	:	";
	c1.printRecord();
	cout << "c2	:	";
	c2.printRecord();
	return 0;
}


/////////////////Pre_Increment_Decrement///////////////
/*int main( void )
{
	Complex c1( 10, 20 );
	cout << "c1	:	";
	c1.printRecord();
	cout << endl;

	Complex c2;

	cout << "Pre_Increment" << endl;
	c2 = ++ c1;	//c2 = operator++( c1 );
	cout << "c1	:	";
	c1.printRecord();
	cout << "c2	:	";
	c2.printRecord();

	cout << "Pre_Decrement" << endl;
	c2 = -- c1;	//c2 = operator--( c1 );
	cout << "c1	:	";
	c1.printRecord();
	cout << "c2	:	";
	c2.printRecord();
	return 0;
}*/

