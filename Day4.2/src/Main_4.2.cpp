/*#include<stdio.h>

int num1 = 10; // ambiguity error
int num1 = 20; // ambiguity error
int main( void )
{

	return 0;
}*/

/*#include<stdio.h>
namespace na
{
	int num1 = 10;	//Namespace Scope
}
int main( void )
{
	printf("Num1	:	%d\n", num1); //error
	return 0;
}*/

/*#include<stdio.h>
namespace na
{
	int num1 = 10;	//Namespace Scope
}
int main( void )
{
	printf("Num1	:	%d\n", na::num1); //10
	return 0;
}*/

/*#include<stdio.h>

int main( void )
{
	namespace na  //not allowed in any function //should be global or in another namespace
	{
		int num1 = 10;	//Namespace Scope
	}

	printf("Num1	:	%d\n", na::num1);
	return 0;
}*/

/*#include<stdio.h>

namespace na
{
	int num1 = 10;
}
namespace nb
{
	int num2 = 20;
}
int main( void )
{
	printf("Num1	:	%d\n", na::num1);  //10
	printf("Num2	:	%d\n", nb::num2);  //20
	return 0;
}*/

/*#include<stdio.h>

namespace na
{
	int num1 = 10;
	int num3 = 30;
}
namespace nb
{
	int num2 = 20;
	int num3 = 40;
}
int main( void )
{
	printf("Num1	:	%d\n", na::num1);	//10
	printf("Num3	:	%d\n", na::num3);	//30

	printf("Num2	:	%d\n", nb::num2);	//20
	printf("Num3	:	%d\n", nb::num3);	//40
	return 0;
}*/



