#include<iostream>
using namespace std;

class Complex
{
private:
	int real;
	int imag;
public:
	Complex( void ) : real( 0 ), imag( 0 )
	{
		cout<<"Complex( void )"<<endl;
	}
	Complex( int real, int imag ) : real( real ), imag( imag )
	{
		cout<<"Complex( int real, int imag )"<<endl;
	}
	void printRecord( void )
	{
		cout<<this->real<<"	"<<this->imag<<endl;
	}
	~Complex( void )
	{
		cout<<"~Complex( void )"<<endl;
	}
};

int main( void )
{
	Complex *ptr = new Complex[ 3 ];			//	Allocation of memory for array of objects
	for( int index = 0; index < 3; ++ index )
		ptr[ index ].printRecord();
	delete[] ptr;								//	Complex::~Complex()	//	Destructor
	ptr = NULL;
	return 0;
}

/*int main( void )
{
	Complex *ptr = new Complex( 10, 20 );	//	Complex::Complex(int, int)	//	Parameterized Constructor
	ptr->printRecord();
	delete ptr;								//	Complex::~Complex()	//	Destructor
	return 0;
}*/



/*int main( void )
{
	//Complex *ptr = new Complex;	//Ok	//	Complex::Complex()	//	Parameterless constructor
	Complex *ptr = new Complex();	//	Same as above	//	Complex::Complex()	//	Parameterless constructor
	ptr->printRecord();
	delete ptr;						//	Complex::~Complex()	//	Destructor
	return 0;
}*/
